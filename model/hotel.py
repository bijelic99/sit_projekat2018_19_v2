import model.soba as soba

file_path="data/hoteli.txt"

svi_hoteli = {}

def main():
    
    print(svi_hoteli)
    """
    hotel  = { 
    "id" : 3, 
    "name" : "Hotel Aleksandar", 
    "address" : "Novi Sad",
    "bazen" : False,
    "restoran" : True,
    "prosecna_ocena" : 2.5,
    "sobe" : []
    }
    dodaj_hotel(hotel)
    """

def ucitaj_hotele():
    doc=open(file_path,"r")
    hoteli={}
    i=0
    for line in doc:
        if(i != 0):
            hotel={}
            line=line.strip("\n")
            line=line.split("|")
            if(line[0]=="000000"):
                line[0]=0
            else:
                line[0]=eval(line[0].lstrip('0'))
            hotel["id"]=line[0]
            hotel["name"]=line[1]
            hotel["address"]=line[2]
            hotel["bazen"]=bool(eval(line[3]))
            hotel["restoran"]=bool(eval(line[4]))
            hotel["prosecna_ocena"]=eval(line[5])
            hotel["sobe"]=soba.ucitaj_sobe_za_hotel(hotel["id"])
            hoteli[hotel["id"]]=hotel
        i+=1

    #print(hoteli)
    doc.close()
    return hoteli

def upisi_sve_hotele():
    doc = open(file_path,"w")
    first_line="id|name|address|bazen|restoran|prosecna_ocena"
    doc.write(first_line)
    for key in svi_hoteli.keys():
        hotel = svi_hoteli[key]
        s1=""
        s1+="\n"+'{:06d}'.format(hotel["id"])+"|"+hotel["name"]+"|"+hotel["address"]+"|"+str(int(hotel["bazen"]))+"|"+str(int(hotel["restoran"]))+"|"+'{:.2f}'.format(hotel["prosecna_ocena"])
        doc.write(s1)

    doc.close()


def obrisi_hotel(id):
    if(id in svi_hoteli.keys()):
        svi_hoteli.pop(id)
        upisi_sve_hotele()

def izmeni_hotel(hotel):
    if(hotel["id"] in svi_hoteli.keys()):
        id = hotel["id"]
        svi_hoteli[id]["name"]=hotel["name"]
        svi_hoteli[id]["address"]=hotel["address"]
        svi_hoteli[id]["bazen"]=hotel["bazen"]
        svi_hoteli[id]["restoran"]=hotel["restoran"]
        svi_hoteli[id]["prosecna_ocena"]=hotel["prosecna_ocena"]
        svi_hoteli[id]["sobe"]=hotel["sobe"]
        upisi_sve_hotele()

def dodaj_hotel(hotel):
    hotel["id"]=generate_id()
    doc = open(file_path,"a")
    s1=""
    s1+="\n"+'{:06d}'.format(hotel["id"])+"|"+hotel["name"]+"|"+hotel["address"]+"|"+str(int(hotel["bazen"]))+"|"+str(int(hotel["restoran"]))+"|"+'{:.2f}'.format(hotel["prosecna_ocena"])
    doc.write(s1)
    svi_hoteli[hotel["id"]]=hotel
    doc.close()

def generate_id():
    return max(svi_hoteli.keys())+1


svi_hoteli = ucitaj_hotele()

#main()