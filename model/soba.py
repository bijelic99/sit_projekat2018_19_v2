file_path="data/sobe.txt"
sve_sobe={}
room_type={
    0 : "apartman",
    1 : "jedna_soba"
}
kupatilo={
    0 : "deljeno",
    1 : "zasebno"
}

"""
soba={
    "id":"0",
    "hotel_id":"0",
    "room_number":"0",
    "no_of_beds":"1",
    "type":"0",
    "ac":"0",
    "tv":"0",
    "balcony":"0",
    "bathroom":"0",
    "price_per_night":25
}
"""

def main():
    
    print(sve_sobe)

def ucitaj_sve_sobe():
    sobe={}
    doc = open(file_path,"r")
    i=0
    for line in doc:
        if(i!=0):
            soba={}
            line = line.strip("\n")
            line = line.split("|")
            if(line[0]=="000000"):
                line[0]=0
            else:
                line[0]=eval(line[0].lstrip('0'))
            soba["id"]=line[0]
            if(line[1]=="000000"):
                line[1]=0
            else:
                line[1]=eval(line[1].lstrip('0'))
            soba["hotel_id"]=line[1]
            soba["room_number"]=eval(line[2])
            soba["no_of_beds"]=eval(line[3])
            soba["type"]=eval(line[4])
            soba["ac"]=bool(eval(line[5]))
            soba["tv"]=bool(eval(line[6]))
            soba["balcony"]=bool(eval(line[7]))
            soba["bathroom"]=eval(line[8])
            soba["price_per_night"]=eval(line[9])
            sobe[soba["id"]]=soba
        i+=1


    return sobe

def ucitaj_sobe_za_hotel(id):
    sobe=[]
    for key in sve_sobe.keys():
        if(sve_sobe[key]["hotel_id"]==id):
            sobe.append(sve_sobe[key])
    return sobe

def upisi_sve_sobe():
    doc = open(file_path,"w")
    first_line="id|hotel_id|room_number|no_of_beds|type|ac|tv|balcony|bathroom|price_per_night"
    doc.write(first_line)
    for key in sve_sobe.keys():
        soba=sve_sobe[key]
        s1=""
        s1+="\n"+'{:06d}'.format(soba["id"])+"|"+'{:06d}'.format(soba["hotel_id"])+"|"+str(soba["room_number"])+"|"+str(soba["no_of_beds"])+"|"+str(soba["type"])+"|"+str(int(soba["ac"]))
        s1+="|"+str(int(soba["tv"]))+"|"+str(int(soba["balcony"]))+"|"+str(soba["bathroom"])+"|"+str(soba["price_per_night"])
        print(s1)
        doc.write(s1)

    doc.close()

def obrisi_sobu(id):
    if(id in sve_sobe.keys()):
        sve_sobe.pop(id)
        upisi_sve_sobe()

def izmeni_sobu(soba):
    if(soba["id"] in sve_sobe.keys()):
        id = soba["id"]
        sve_sobe[id]["hotel_id"]=soba["hotel_id"]
        sve_sobe[id]["room_number"]=soba["room_number"]
        sve_sobe[id]["no_of_beds"]=soba["no_of_beds"]
        sve_sobe[id]["type"]=soba["type"]
        sve_sobe[id]["ac"]=soba["ac"]
        sve_sobe[id]["tv"]=soba["tv"]
        sve_sobe[id]["balcony"]=soba["balcony"]
        sve_sobe[id]["bathroom"]=soba["bathroom"]
        sve_sobe[id]["price_per_night"]=soba["price_per_night"]

        upisi_sve_sobe()

def dodaj_sobu(soba):
    soba["id"]=generate_id()
    doc = open(file_path,"a")
    s1=""
    s1+="\n"+'{:06d}'.format(soba["id"])+"|"+'{:06d}'.format(soba["hotel_id"])+"|"+str(soba["room_number"])+"|"+str(soba["no_of_beds"])+"|"+str(soba["type"])+"|"+str(int(soba["ac"]))
    s1+="|"+str(int(soba["tv"]))+"|"+str(int(soba["balcony"]))+"|"+str(soba["bathroom"])+"|"+str(soba["price_per_night"])
    print(s1)
    doc.write(s1)
    doc.close()
    sve_sobe[soba["id"]]=soba

def get_soba(id):
    if(id in sve_sobe.keys()):
        return sve_sobe[id]
    else:
        raise Exception("Soba ne postoji")

def generate_id():
    return max(sve_sobe.keys())+1
    


sve_sobe = ucitaj_sve_sobe()

#main()