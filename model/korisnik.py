from copy import deepcopy
file_path="data/korisnici.txt"
file_path_recepcioner="data/recepcioneri.txt"


"""
korisnik={
            id : "Generisan Kasnije",
            username : "",
            password : "",
            name : "",
            surname : "",
            phone : "",
            email : "",
            role : "Registrovan korisnik"
        }
"""

roles = {
    0 : "ne_registrovan",
    1 : "registrovan",
    2 : "recepcioner",
    3 : "admin"
}
svi_korisnici={}

def main():
    
    print(svi_korisnici)






def ucitaj_korisnike():
    doc = open(file_path,"r")
    korisnici = {}
    i=0
    for line in doc:
        if(i != 0):
            korisnik = {}
            line = line.strip("\n")
            line = line.split("|")
            line[0] = eval(line[0])
            korisnik["id"] = line[0]
            korisnik["username"] = line[1]
            korisnik["password"] = line[2]
            korisnik["name"] = line[3]
            korisnik["surname"] = line[4]
            korisnik["phone"] = line[5]
            korisnik["email"] = line[6]
            korisnik["role"] = eval(line[7])
            if(korisnik["role"]==2):
                korisnik["hotel_id"]=get_Recepcioner_hotel(korisnik["id"])
            korisnici[korisnik["id"]]=korisnik
            
        i+=1
    #print(korisnici)
    doc.close()
    return korisnici

def upisi_sve_korisnike():
    doc = open(file_path,"w")  
    first_line="id|username|password|name|surname|phone|email|role"
    doc.write(first_line)
    for id in svi_korisnici.keys():
        korisnik = svi_korisnici[id]
        s1 = ""
        s1+="\n"+str(korisnik["id"])+"|"+korisnik["username"]+"|"+korisnik["password"]+"|"
        s1+=korisnik["name"]+"|"+korisnik["surname"]+"|"+korisnik["phone"]+"|"+korisnik["email"]+"|"+str(korisnik["role"])
        doc.write(s1)

    doc.close()
def upisi_hotel_id_recepcionera(rec_id,hotel_id):
    doc = open(file_path_recepcioner,"r")
    arr=[]
    i=0
    for line in doc:
        if(i!=0):
            line=line.strip("\n")
            line=line.split("|")
            line[0]=eval(line[0])
            line[1]=eval(line[1])
            arr.append(line)
        i+=1
    doc.close()
    if([rec_id,hotel_id] not in arr):
        doc = open(file_path_recepcioner,"a")
        s="\n"+str(rec_id)+"|"+str(hotel_id)
        doc.write(s)
        doc.close()
def obrisi_korisnika(id):
    if(id in svi_korisnici.keys()):
        svi_korisnici.pop(id)
        upisi_sve_korisnike()

def izmeni_korisnika(korisnik):
    if(korisnik["id"] in svi_korisnici.keys()):
        id  = korisnik["id"]
        svi_korisnici[id]["username"]=korisnik["username"]
        svi_korisnici[id]["password"]=korisnik["password"]
        svi_korisnici[id]["name"]=korisnik["name"]
        svi_korisnici[id]["surname"]=korisnik["surname"]
        svi_korisnici[id]["phone"]=korisnik["phone"]
        svi_korisnici[id]["email"]=korisnik["email"]
        svi_korisnici[id]["role"]=korisnik["role"]
        if(korisnik["role"]==2):
            svi_korisnici[id]["hotel_id"]=korisnik["hotel_id"]
            upisi_hotel_id_recepcionera(korisnik["id"],korisnik["hotel_id"])
        upisi_sve_korisnike()

def dodaj_korisnika(korisnik):
    doc = open(file_path,"a")
    korisnik["id"]=generate_id()
    svi_korisnici[korisnik["id"]]=korisnik
    s1 = ""
    s1+="\n"+str(korisnik["id"])+"|"+korisnik["username"]+"|"+korisnik["password"]+"|"
    s1+=korisnik["name"]+"|"+korisnik["surname"]+"|"+korisnik["phone"]+"|"+korisnik["email"]+"|"+str(korisnik["role"])
    doc.write(s1)

    doc.close()

def generate_id():
    id = max(svi_korisnici.keys())+1
    return id

def get_user(username, pwd):
    for key in svi_korisnici.keys():
        if(svi_korisnici[key]["username"]==username and svi_korisnici[key]["password"]==pwd):
            return deepcopy(svi_korisnici[key])

    return False

def get_Recepcioner_hotel(id):
    doc = open(file_path_recepcioner,"r")
    arr = []
    i=0
    for line in doc:
        if(i!=0):
            line=line.strip("\n")
            line=line.split("|")
            for i in range(len(line)):
                
                line[i]=eval(line[i])
            arr.append(line)
        arr=list(filter(lambda line: line[0]==id,arr))
        if(len(arr)==1):
            return arr[0][1]
        i+=1

svi_korisnici = ucitaj_korisnike()

def obrisi_Recepcioner_hotel(recepcioner_id):
    doc = open(file_path_recepcioner, "r")
    arr = []
    i = 0
    for line in doc:
        if i !=0:
            line = line.strip("\n")
            line = line.split("|")
            line[0],line[1]=eval(line[0]),eval(line[1])
            arr.append(line)
        i+=1
    doc.close()
    arr = list(filter(lambda x: x[0] != recepcioner_id, arr))
    doc = open(file_path_recepcioner, "w")
    s="recepcioner_id|hotel_id"
    doc.write(s)
    for x in arr:
        s = "\n"+str(x[0])+"|"+str(x[1])
        doc.write(s)

#main()




