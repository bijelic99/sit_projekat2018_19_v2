import model.soba as soba
import model.hotel
import datetime

file_path="data/rezervacije.txt"
file_path_2="data/rezervacije_sobe.txt"
sve_rezervacije={}
statusi_rezervacije={
    0 : "nije zapoceta",
    1 : "u toku",
    2 : "zavrsena"
}
"""
rezervacija={
    "id":"",
    "datum_i_vreme_rezervacije" : "",
    "datum_prijave" : "",
    "datum_odjave":"",
    "username_korisnika":"",
    "status_rezervacije":"",
    "ocena_hotela":"",
    "hotel_id":"",
    "rezervisane_sobe":[]

}

"""


def main():
    
    print(sve_rezervacije)

def ucitaj_sve_rezervacije():
    rezervacije = {}
    doc = open(file_path,"r")
    i = 0
    for line in doc:
        if(i!=0):
            rezervacija = {}
            line = line.strip("\n")
            line = line.split("|")
            if(line[0]=="000000"):
                line[0]=0
            else:
                line[0]=eval(line[0].lstrip('0'))
            rezervacija["id"]=line[0]
            line[1]=line[1].split(" ")
            line[1][0]=line[1][0].split("-")
            line[1][1]=line[1][1].split(":")
            """
            pisano ovako zbog problema sa eval("00".lstrip("0"))
            """
            for i in range(len(line[1][0])):
                    if(line[1][0][i] in ["0","00","000","0000"]):
                        line[1][0][i]=0
                    else:
                        try:
                            line[1][0][i]=eval(line[1][0][i].lstrip("0"))
                        except:
                            line[1][0][i]=0
            for i in range(len(line[1][1])):
                    if(line[1][1][i] in ["0","00","000","0000"]):
                        line[1][1][i]=0
                    else:
                        try:
                            line[1][1][i]=eval(line[1][1][i].lstrip("0"))
                        except:
                            line[1][1][i]=0
            #print(line[1][0])
            rezervacija["datum_i_vreme_rezervacije"]=datetime.datetime(line[1][0][0], line[1][0][1], line[1][0][2], line[1][1][0], line[1][1][1], line[1][1][2])
            line[2]=line[2].split("-")
            for i in range(len(line[2])):
                    if(line[2][i] in ["0","00","000","0000"]):
                        line[2][i]=0
                    else:
                        try:
                            line[2][i]=eval(line[2][i].lstrip("0"))
                        except:
                            line[2][i]=0
            #print(line[2])
            rezervacija["datum_prijave"]=datetime.datetime(line[2][0],line[2][1],line[2][2])
            line[3]=line[3].split("-")
            for i in range(len(line[3])):
                    if(line[3][i] in ["0","00","000","0000"]):
                        line[3][i]=0
                    else:
                        try:
                            line[3][i]=eval(line[3][i].lstrip("0"))
                        except:
                            line[3][i]=0
            #print(line[3])
            rezervacija["datum_odjave"]=datetime.datetime(line[3][0],line[3][1],line[3][2])
            rezervacija["username_korisnika"]=line[4]
            rezervacija["status_rezervacije"]=eval(line[5])           
            rezervacija["ocena_hotela"]=eval(line[6])
            if(line[7]=="000000"):
                line[7]=0
            else:
                line[7]=eval(line[7].lstrip('0'))
            rezervacija["hotel_id"]=line[7]
            rezervacija["rezervisane_sobe"]=rezervisane_sobe(rezervacija["id"])
            rezervacije[rezervacija["id"]]=rezervacija



        i+=1






    doc.close()
    return rezervacije

def rezervisane_sobe(id_rezervacije):
    sobe_za_rez=[]
    doc = open(file_path_2,"r")
    i = 0
    for line in doc:
        if i!=0:
            line=line.strip("\n")
            line=line.split("|")
            if(line[0]=="000000"):
                line[0]=0
            else:
                line[0]=eval(line[0].lstrip("0"))
            if(line[1]=="000000"):
                line[1]=0
            else:
                line[1]=eval(line[1].lstrip("0"))
            #print(line[1])
            if(line[0]==id_rezervacije):
                soba1 = soba.get_soba(line[1])
                sobe_za_rez.append(soba1)
           
        i+=1
        


    doc.close()
    return sobe_za_rez

"""
def upisi_rezervisane_sobe(id_rez,sobe):
    rezervisane_sobe=[]
    doc = open(file_path_2,"r")
    i=0
    for line in doc:
        if(i!=0):
            line=line.strip("\n")
            line=line.split("|")
            if(line[0]=="000000"):
                line[0]=0
            else:
                line[0]=eval(line[0].lstrip("0"))
            if(line[1]=="000000"):
                line[1]=0
            else:
                line[1]=eval(line[1].lstrip("0"))
            rezervisane_sobe.append([line[0],line[1]])
        i+=1
    doc.close()
    for soba1 in sobe:
        if [id_rez,soba1["id"]] not in rezervisane_sobe:
            rezervisane_sobe.append([id_rez,soba1["id"]])
    doc = open(file_path_2,"w")
    first_line="id_rezervacije|id_sobe"
    doc.write(first_line)
    for r in rezervisane_sobe:
        s1=""
        s1="\n"+r[0]+"|"+r[1]
        doc.write(s1)


    doc.close()
"""
def upisi_rezervisane_sobe(id_rez,sobe,i=0):
    if(i==0):
        if(len(sobe)>0):
            doc = open(file_path_2,"a")
            for so in sobe:
                s=""
                s+="\n"+'{:06d}'.format(id_rez)+"|"+'{:06d}'.format(so["id"])
                doc.write(s)


            doc.close()
    elif(i==1):
        if(len(sobe)>0):
            doc = open(file_path_2,"w")
            s="id_rezervacije|id_sobe"
            doc.write(s)
            for so in sobe:
                s=""
                s+="\n"+'{:06d}'.format(id_rez)+"|"+'{:06d}'.format(so["id"])
                doc.write(s)


            doc.close()


def upisi_rezervaciju(rezervacija):
    rezervacija["id"]=generate_id()
    doc=open(file_path,"a")
    s1=""
    """
    id|datum_i_vreme_rezervacije|datum_prijave|datum_odjave|username_korisnika|status_rezervacije|ocena_hotela|hotel_id
    """
    s1+="\n"+'{:06d}'.format(rezervacija["id"])+"|"+rezervacija["datum_i_vreme_rezervacije"].strftime("%Y-%m-%d %H:%M:%S")+"|"+rezervacija["datum_prijave"].strftime("%Y-%m-%d")+"|"+rezervacija["datum_odjave"].strftime("%Y-%m-%d")+"|"
    s1+=rezervacija["username_korisnika"]+"|"+str(rezervacija["status_rezervacije"])+"|"+str(rezervacija["ocena_hotela"])+"|"+'{:06d}'.format(rezervacija["hotel_id"])
    sve_rezervacije[rezervacija["id"]]=rezervacija
    doc.write(s1)
    doc.close()
    upisi_rezervisane_sobe(rezervacija["id"],rezervacija["rezervisane_sobe"])

def upisi_sve_rezervacije():
    doc=open(file_path,"w")
    first_line="id|datum_i_vreme_rezervacije|datum_prijave|datum_odjave|username_korisnika|status_rezervacije|ocena_hotela|hotel_id"
    doc.write(first_line)
    #i je potrebno zbog ponovnog upisa dokumenta rezervacija_sobe.txt
    i=1
    for key in sve_rezervacije.keys():
        rezervacija = sve_rezervacije[key]
        s1=""
        s1+="\n"+'{:06d}'.format(rezervacija["id"])+"|"+rezervacija["datum_i_vreme_rezervacije"].strftime("%Y-%m-%d %H:%M:%S")+"|"+rezervacija["datum_prijave"].strftime("%Y-%m-%d")+"|"+rezervacija["datum_odjave"].strftime("%Y-%m-%d")+"|"
        s1+=rezervacija["username_korisnika"]+"|"+str(rezervacija["status_rezervacije"])+"|"+str(rezervacija["ocena_hotela"])+"|"+'{:06d}'.format(rezervacija["hotel_id"])
        doc.write(s1)
        upisi_rezervisane_sobe(rezervacija["id"],rezervacija["rezervisane_sobe"],i)
        i=0
    doc.close()
def obrisi_rezervaciju(id):
    if(id in sve_rezervacije.keys()):
        sve_rezervacije.pop(id)
        upisi_sve_rezervacije()

def izmeni_rezervaciju(rezervacija):
    if(rezervacija["id"] in sve_rezervacije.keys()):
        sve_rezervacije[rezervacija["id"]]=rezervacija
        upisi_sve_rezervacije()





def generate_id():
    return max(sve_rezervacije.keys())+1

def proveri_tok_rezervacija():
    trenutno_vreme = datetime.datetime.now()
    for key in sve_rezervacije.keys():
        if((sve_rezervacije[key]["datum_odjave"]).date()<=trenutno_vreme.date()):
            sve_rezervacije[key]["status_rezervacije"]=2
        elif((sve_rezervacije[key]["datum_odjave"]).date()>=trenutno_vreme.date() and (sve_rezervacije[key]["datum_prijave"]).date()<=trenutno_vreme.date()):
            sve_rezervacije[key]["status_rezervacije"]=1
        elif((sve_rezervacije[key]["datum_odjave"]).date()>=trenutno_vreme.date() and (sve_rezervacije[key]["datum_prijave"]).date()>=trenutno_vreme.date()):
            sve_rezervacije[key]["status_rezervacije"]=0
    upisi_sve_rezervacije()

def oceni_hotele():
    for key in model.hotel.svi_hoteli.keys():
        lista_rez=get_rezervacije_za_hotel(key)
        i = 0
        s = 0
        for rez in lista_rez:
            if(rez["ocena_hotela"]>0):
                s+=rez["ocena_hotela"]
                i+=1
        if(i>0):
            avg=s/i
            model.hotel.svi_hoteli[key]["prosecna_ocena"]=avg
            model.hotel.upisi_sve_hotele()
            model.hotel.svi_hoteli=model.hotel.ucitaj_hotele()

    return False

def get_rezervacije_za_hotel(hotel_id):
    lst = []
    lst=list(filter(lambda rezervacija: rezervacija["hotel_id"]==hotel_id,sve_rezervacije.values()))
    

    return lst



sve_rezervacije = ucitaj_sve_rezervacije()
proveri_tok_rezervacija()
oceni_hotele()
#main()