from utility import *
import controller.send_to as send_to
import controller.recepcioner as control
import view.registrovan_view

def show(user):
    clear_screen()
    print_in_mid(" Recepcioner ","*")
    tekstualni_meni={
    0 : "Pretraga Soba",
    1 : "Pretraga rezervacija",
    2 : "Izvestavanje",
    3 : "Odjava"
    }
    meni={
    0 : pretraga_soba,
    1 : pretraga_rezervacija,
    2 : Izvestavanje,
    3 : odjava
    }
    
    meni[input_by_choice(tekstualni_meni)](user)

def odjava(user={"role":0}):
    user["role"]=0
    
    clear_screen()
    send_to.send_to_view(user)


def pretraga_soba(user):
    clear_screen()
    print_in_mid(" Pretraga soba ","*")
    tekstualni_meni={
    0 : "Po jednom kriterijumu",
    1 : "Po vise kriterijuma"
    }
    meni={
    0 : pretraga_soba_po_jednom_kriterijumu,
    1 : pretraga_soba_po_vise_kriterijuma
    }
    
    meni[input_by_choice(tekstualni_meni)](user)

def pretraga_soba_po_jednom_kriterijumu(user):
    kriterijumi={
        1 : "room_number",
        2 : "no_of_beds",
        3 : "type",
        4 : "ac",
        5 : "tv",
        6 : "balcony",
        7 : "bathroom",
        8 : "price_per_night",
        9 : "dostupne_u_periodu",
        0 : "nazad"

    }
    clear_screen()
    print_in_mid("Pretraga po jednom kriterijumu","*")
    izabrano = input_by_choice(kriterijumi)
    clear_screen()
    if(izabrano !=0 and izabrano != 9):
        while True:
            clear_screen()
            s="Unesite "+kriterijumi[izabrano]+": "
            value = input(s)
            if(control.pretraga_soba_po_jednom_kriterijumu_validate(izabrano,value)):
                break
        sobe = control.get_soba_jedan_kriterijum(user["hotel_id"],kriterijumi[izabrano],value)
        sobe = control.format_sobe_for_print(sobe)
        clear_screen()
        for soba in sobe:
            print_in_mid(soba)

        input("Unesite bilo sta za povratak na prethodni meni: ")
    elif(izabrano == 9):
        while True:
            clear_screen()
            datum_od = input("Unesite datum OD u obliku dd-mm-gggg: ")
            datum_do = input("Unesite datum DO u obliku dd-mm-gggg: ")
            if(control.datumi_za_pretragu_soba_validate(datum_od,datum_do)):
                break
        datum_od,datum_do = control.datumi_za_pretragu_soba_parse(datum_od,datum_do)
        sobe = control.get_soba_slobodnih_za_datume(user["hotel_id"], datum_od, datum_do)
        sobe = control.format_sobe_for_print(sobe)
        clear_screen()
        for soba in sobe:
            print_in_mid(soba)

        input("Unesite bilo sta za povratak na prethodni meni: ")



    send_to.send_to_view(user)

def pretraga_soba_po_vise_kriterijuma(user):
    kriterijumi={

        "room_number" : "unset",
        "no_of_beds" : "unset",
        "type" : "unset",
        "ac" : "unset",
        "tv" : "unset",
        "balcony" : "unset",
        "bathroom" : "unset",
        "price_per_night" : "unset"

    }
    clear_screen()
    print_in_mid("Pretraga po vise kriterijuma","*")
    while True:
        kriterijumi = fill_dict_with_str_2(kriterijumi,[],[],"Pretraga po vise kriterijuma; za ac,tv,balcony uneti true ili false")
        clear_screen()
        if(control.vise_kriterijumska_pretraga_soba_validate(kriterijumi)):
            break
    kriterijumi = control.vise_kriterijumska_pretraga_soba_parse(kriterijumi)
    if(y_n_question("Dali vas interesuju sobe samo za odredjen period?")):
        while True:
            clear_screen()
            datum_od = input("Unesite datum OD u obliku dd-mm-gggg: ")
            datum_do = input("Unesite datum DO u obliku dd-mm-gggg: ")
            if(control.datumi_za_pretragu_soba_validate(datum_od,datum_do)):
                break
        datum_od,datum_do = control.datumi_za_pretragu_soba_parse(datum_od,datum_do)
        sobe = control.get_soba_slobodnih_za_datume(user["hotel_id"], datum_od, datum_do)
        sobe = control.get_soba_vise_kriterijum(user["hotel_id"],kriterijumi,sobe)
        sobe = control.format_sobe_for_print(sobe)
        clear_screen()
        for soba in sobe:
            print_in_mid(soba)

        input("Unesite bilo sta za povratak na prethodni meni: ")
    else:
        sobe = control.get_soba_vise_kriterijum(user["hotel_id"],kriterijumi)
        sobe = control.format_sobe_for_print(sobe)
        clear_screen()
        for soba in sobe:
            print_in_mid(soba)

        input("Unesite bilo sta za povratak na prethodni meni: ")


    send_to.send_to_view(user)

def pretraga_rezervacija(user):
    clear_screen()
    print_in_mid(" Pretraga rezervacija ","*")
    tekstualni_meni={
    0 : "Po jednom kriterijumu",
    1 : "Po vise kriterijuma"
    }
    meni={
    0 : pretraga_rezervacija_po_jednom_kriterijumu,
    1 : pretraga_rezervacija_po_vise_kriterijuma
    }
    
    meni[input_by_choice(tekstualni_meni)](user)

def pretraga_rezervacija_po_jednom_kriterijumu(user):
    kriterijumi={
        1 : "Datum kreacije",
        2 : "Datum prijave",
        3 : "Datum odjave",
        4 : "Korisnik koji je kreirao",
        5 : "Status rezervacije",
        0 : "nazad"

    }
    clear_screen()
    print_in_mid("Pretraga po jednom kriterijumu","*")
    izabrano = input_by_choice(kriterijumi)
    clear_screen()
    if(izabrano !=0):
        while True:
            clear_screen()
            s="Unesite "+kriterijumi[izabrano]+": "
            value = input(s)
            if(control.pretraga_rezervacija_po_jednom_kriterijumu_validate(izabrano,value)):
                break
        value = control.pretraga_rezervacija_po_jednom_kriterijumu_parse(izabrano,value)
        rezervacije=control.get_pretraga_rezervacija_po_jednom_kriterijumu(user["hotel_id"],izabrano,value)
        #log_something(rezervacije)
        print_dict(view.registrovan_view.control.make_readable_dict(rezervacije.values()))
        

        input("Unesite bilo sta za povratak na prethodni meni: ")

    send_to.send_to_view(user)

def pretraga_rezervacija_po_vise_kriterijuma(user):
    kriterijumi={

        "Datum kreacije" : "unset",
        "Datum prijave" : "unset",
        "Datum odjave" : "unset",
        "Korisnik koji je kreirao" : "unset",
        "Status rezervacije" : "unset"

    }
    clear_screen()
    print_in_mid("Pretraga po vise kriterijuma","*")
    while True:
        kriterijumi = fill_dict_with_str_2(kriterijumi,[],[],"Pretraga po vise kriterijuma; za datume format dd-mm-yyyy")
        clear_screen()
        if(control.vise_kriterijumska_pretraga_rezervacija_validate(kriterijumi)):
            break
    kriterijumi = control.vise_kriterijumska_pretraga_rezervacija_parse(kriterijumi)
    #log_something(kriterijumi)
    rezervacije = control.get_vise_kriterijumska_pretraga_rezervacija(user["hotel_id"],kriterijumi)
    #log_something(rezervacije)
    print_dict(view.registrovan_view.control.make_readable_dict(rezervacije))
    
    input("Unesite bilo sta za povratak na prethodni meni: ")    

    send_to.send_to_view(user)

def Izvestavanje(user):
    clear_screen()
    print_in_mid(" Izvestavanje ","*")
    tekstualni_meni={
    0 : "Dnevini nivo",
    1 : "Nedeljni Nivo",
    2 : "Mesecni nivo",
    }
    meni={
    0 : dnevni_izvestaj,
    1 : nedeljni_izvestaj,
    2 : mesecni_izvestaj
    }
    
    meni[input_by_choice(tekstualni_meni)](user)
"""
period={
    0 : "Dnevni",
    1 : "Nedeljni",
    2 : "Mesecni"
}
"""
def dnevni_izvestaj(user):
    clear_screen()
    print_in_mid(" Dnevni Izvestaj ","*")
    period=0
    tekstualni_meni={
    0 : "Listu realizovanih rezervacija",
    1 : "Ukupan broj realizovanih rezervacija",
    2 : "Ukupan broj izdatih soba",
    4 : "Ukupna zarada za dati period",
    5 : "Prosečna ocena hotela za dati period"
    }
    meni={
    0 : realizovane_rezervacije,
    1 : broj_realizovanih_rezervacija,
    2 : broj_izdatih_soba,
    4 : zarada,
    5 : prosecna_ocena_hotela
    }
    
    meni[input_by_choice(tekstualni_meni)](user,period)
    
def nedeljni_izvestaj(user):
    clear_screen()
    print_in_mid(" Nedeljni Izvestaj ","*")
    period=1
    tekstualni_meni={
    0 : "Listu realizovanih rezervacija",
    1 : "Ukupan broj realizovanih rezervacija",
    2 : "Ukupan broj izdatih soba",
    4 : "Ukupna zarada za dati period",
    5 : "Prosečna ocena hotela za dati period"
    }
    meni={
    0 : realizovane_rezervacije,
    1 : broj_realizovanih_rezervacija,
    2 : broj_izdatih_soba,
    4 : zarada,
    5 : prosecna_ocena_hotela
    }
    
    meni[input_by_choice(tekstualni_meni)](user,period)

def mesecni_izvestaj(user):
    clear_screen()
    print_in_mid(" Mesecni Izvestaj ","*")
    period=2
    tekstualni_meni={
    0 : "Listu realizovanih rezervacija",
    1 : "Ukupan broj realizovanih rezervacija",
    2 : "Ukupan broj izdatih soba",
    4 : "Ukupna zarada za dati period",
    5 : "Prosečna ocena hotela za dati period"
    }
    meni={
    0 : realizovane_rezervacije,
    1 : broj_realizovanih_rezervacija,
    2 : broj_izdatih_soba,
    4 : zarada,
    5 : prosecna_ocena_hotela
    }
    
    meni[input_by_choice(tekstualni_meni)](user,period)

def realizovane_rezervacije(user,period):
    rezervacije = control.get_rezervacije_za_period(user,period)
    #log_something("rezervacije")
    #log_something(rezervacije)
    print_dict(view.registrovan_view.control.make_readable_dict(list(rezervacije.values())))
    input("Unesite bilo sta za povratak na prethodni meni: ")

    send_to.send_to_view(user)

def broj_realizovanih_rezervacija(user,period):
    clear_screen()
    print_in_mid("Broj realizovanih rezervacija","*")
    print(" ")
    print_in_mid(str(control.get_broj_realizovanih_rezervacija(user,period))," ")
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to.send_to_view(user)

def broj_izdatih_soba(user,period):
    clear_screen()
    print_in_mid("Broj izdatih soba za period","*")
    print(" ")
    print_in_mid(str(control.get_broj_izdatih_soba(user,period))," ")
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to.send_to_view(user)

def zarada(user,period):
    clear_screen()
    print_in_mid("Zarada za period","*")
    print(" ")
    print_in_mid(str(control.get_zarada(user,period))+" jedinica novcane vrednosti"," ")
    input("Unesite bilo sta za povratak na prethodni meni: ")
    
    send_to.send_to_view(user)

def prosecna_ocena_hotela(user,period):
    clear_screen()
    print_in_mid("Prosecna ocena za period","*")
    print(" ")
    print_in_mid("{:0.2f}".format(float(control.get_prosecna_ocena(user,period)))," ")
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to.send_to_view(user)
