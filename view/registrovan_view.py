import view.ne_registrovan_view as ne_reg
from utility import *
import controller.registrovan_korisnik as control
import controller.send_to as send_to

current_user={}
meni={}
tekstualni_meni={}

def show(user):
    
    current_user=user
    clear_screen()
    print_in_mid("Registrovan korisnik","*")
    tekstualni_meni={
    0 : "Pregled hotela",
    1 : "Pretraga hotela",
    2 : "Prikaz najbolje ocenjenih hotela",
    3 : "Kreiranje rezervacije",
    4 : "Pregled rezervacija",
    5 : "Ocenjivanje hotela",
    6 : "Odjava"
    }
    meni={
    0 : ne_reg.pregled_hotela,
    1 : ne_reg.pretraga_hotela,
    2 : ne_reg.prikaz_najbolje_ocenjenih_hotela,
    3 : kreiranje_rezervacije,
    4 : pregled_rezervacija,
    5 : ocenjivanje_hotela,
    6 : odjava
    }
    
    meni[input_by_choice(tekstualni_meni)](user)

def kreiranje_rezervacije(user=current_user):
    rezervacija={
    "id":"",
    "datum_i_vreme_rezervacije" : "",
    "datum_prijave" : "",
    "datum_odjave":"",
    "username_korisnika":"",
    "status_rezervacije":"",
    "ocena_hotela":"",
    "hotel_id":"",
    "rezervisane_sobe":[]
    }

    clear_screen()
    print_in_mid("Kreiranje rezervacije","*")
    if (y_n_question("Jeste li sigurni da hocete da napravite novu rezervaciju?")):
        clear_screen()
        print_in_mid("Izaberite hotel","*")
        hoteli=control.getHotels()
        hotel = hoteli[input_by_choice(control.make_hotels_readable_dict(hoteli))]
        rezervacija["hotel_id"]=hotel["id"]
        clear_screen()
        print_in_mid("Kreiranje rezervacije","*")
        while True:
            a=input("Unesite datum od kog zelite da traje rezervacija u obliku dd-mm-yyyy: ")
            if(control.is_date_valid_for_new_res(a)):
                break
        rezervacija["datum_prijave"]=control.make_date(a)
        while True:
            a=input("Unesite koliko dana zelite da ostanete: ")
            try:
                a = eval(a)
                if(a>0):
                    rezervacija["datum_odjave"]=control.get_last_day(rezervacija["datum_prijave"],a)
                    break
            except:
                continue
        clear_screen()
        sobe=control.make_rooms_readable_dict(control.get_available_rooms(hotel["id"],rezervacija["datum_prijave"],rezervacija["datum_odjave"]))
        sobe = input_by_choice_return_multiple(sobe,text="Unesite sobe koje hocete ili d za kraj",filler="*")
        rezervacija["rezervisane_sobe"]=sobe
        rezervacija["username_korisnika"]=user["username"]
        rezervacija["status_rezervacije"]=0
        rezervacija["ocena_hotela"]=0
        control.napravi_rezervaciju(rezervacija)
        display_msg("Uspesna Rezervacija","vracamo vas na prethodnu stranicu","*",1)
        send_to.send_to_view(user)
    else:
        send_to.send_to_view(user)
    
    return False

def pregled_rezervacija(user=current_user):
    current_user=user
    clear_screen()
    print_in_mid("Pregled Rezervacija","*")
    tekstualni_meni={
    0 : "Prosle Rezervacije",
    1 : "Trenutne Rezervacije",
    2 : "Buduce Rezervacije",
    3 : "Sve Rezervacije",
    
    }
    meni={
    0 : prethodne_rezervacije,
    1 : rezervacije_u_toku,
    2 : buduce_rezervacije,
    3 : sve_rezervacije,
    
    }
    
    meni[input_by_choice(tekstualni_meni)](user)
    
    

def ocenjivanje_hotela(user=current_user):
    clear_screen()
    print_in_mid("Ocena Hotela","*")
    dostupne_rezervacije_za_ocenu = control.get_users_past_reservations(user["username"])
    readable_dict = control.make_readable_dict(dostupne_rezervacije_za_ocenu)
    readable_dict[999] = "nazad"
    print_in_mid("Izaberite zavrsenu rezervaciju za koju zelite da ocneite hotel","*")
    rez_id = input_by_choice(readable_dict)
    if rez_id != 999:
        rezervacija = list(filter(lambda rez: rez["id"]==rez_id, dostupne_rezervacije_za_ocenu))
        while True:
            try:
                ocena = eval(input("Unesite ocenu od 1 do 5: "))
                if(ocena in range(1,6)):
                    break
            except:
                continue
        control.oceni_hotel(rezervacija[0],ocena)
        display_msg("Uspesna Rezervacija","vracamo vas na prethodnu stranicu","*",1)
    send_to.send_to_view(user)

def prethodne_rezervacije(user=current_user):
    clear_screen()
    print_in_mid("Pregled zavrsenih rezervacija","*")
    korisnikove_rezervacije = control.get_users_past_reservations(user["username"])
    print_dict(control.make_readable_dict(korisnikove_rezervacije))
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to.send_to_view(user)
    

def buduce_rezervacije(user=current_user):
    clear_screen()
    print_in_mid("Pregled buducih rezervacija","*")
    korisnikove_rezervacije = control.get_users_future_reservations(user["username"])
    print_dict(control.make_readable_dict(korisnikove_rezervacije))
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to.send_to_view(user)
    

def rezervacije_u_toku(user=current_user):
    clear_screen()
    print_in_mid("Pregled trenutnih rezervacija","*")
    korisnikove_rezervacije = control.get_users_current_reservations(user["username"])
    print_dict(control.make_readable_dict(korisnikove_rezervacije))
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to.send_to_view(user)
    

def sve_rezervacije(user=current_user):
    clear_screen()
    print_in_mid("Pregled svih rezervacija","*")
    korisnikove_rezervacije = control.get_users_reservations(user["username"])
    print_dict(control.make_readable_dict(korisnikove_rezervacije))
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to.send_to_view(user)
    
def odjava(user={"role":0}):
    user["role"]=0
    
    clear_screen()
    send_to.send_to_view(user)

