from utility import *
import view.ne_registrovan_view as ne_reg
import controller.send_to as send_to
import controller.admin as control


def show(user):
    clear_screen()
    print_in_mid(" Admin ","*")
    tekstualni_meni={
    0 : "Pregled hotela",
    1 : "Pretraga hotela",
    2 : "Prikaz najbolje ocenjenih hotela",
    3 : "Dodavanje hotela",
    4 : "Dodavanje Recepcionera u hotele",
    5 : "Azuriranje Hotela",
    6 : "Brisanje Hotela",
    7 : "Brisanje recepcionera",
    8 : "Pretraga recepcionera",
    9 : "Odjava"
    }

    meni={
    0 : ne_reg.pregled_hotela,
    1 : ne_reg.pretraga_hotela,
    2 : ne_reg.prikaz_najbolje_ocenjenih_hotela,
    3 : dodavanje_hotela,
    4 : dodavanje_recepcionera_u_hotel,
    5 : hotel_update,
    6 : hotel_delete,
    7 : brisanje_recepcionera,
    8 : pretraga_recepcionera,
    9 : odjava
    }
    
    meni[input_by_choice(tekstualni_meni)](user)


def odjava(user={"role":0}):
    user["role"]=0
    
    clear_screen()
    send_to.send_to_view(user)

def dodavanje_hotela(user):
    hotel  = { 
    "id" : "", 
    "name" : "", 
    "address" : "",
    "bazen" : "",
    "restoran" : "",
    "prosecna_ocena" : ""
    
    }
    clear_screen()
    print_in_mid("Dodavanje hotela","*")
    if (y_n_question("Jeste li sigurni da hocete da napravite novi hotel?")):
        while not control.check_if_new_hotel_is_valid(hotel):
            hotel=fill_dict_with_str_2(hotel,["id"],["name","address","bazen","restoran","prosecna_ocena"])
            clear_screen()
        a=0
        while True :
            try:
                a=eval(input("Unesite broj soba koliko zelite da unesete (bar jednu): "))
                if(a<1):
                    continue
                else:
                    break
            except:
                continue
        hotel = control.add_hotel(hotel)
        sobe=[]
        for i in range(a):
            sobe.append(dodavanje_sobe_u_hotel(hotel["id"],i))
        control.dodavanje_soba_hotela(hotel["id"],sobe)
        display_msg("Uspesno ste dodali hotel"," "," ",1)
    else:
        send_to.send_to_view(user)
    send_to.send_to_view(user)

def dodavanje_sobe_u_hotel(hotel_id, br_sobe=0):
    clear_screen()
    s="Dodavanje sobe"+str(br_sobe)
    print_in_mid(s,"*")
    soba={
    "id":"0",
    "hotel_id":"0",
    "room_number":"0",
    "no_of_beds":"1",
    "type":"0",
    "ac":"0",
    "tv":"0",
    "balcony":"0",
    "bathroom":"0",
    "price_per_night":"0"
    }
    soba["hotel_id"]=str(hotel_id)
    while not control.check_if_new_room_is_valid(soba):
            soba=fill_dict_with_str_2(soba,["id","hotel_id"],["room_number","no_of_beds","type","ac","tv","balcony","bathroom","price_per_night"],s)
            clear_screen()
    return soba



def dodavanje_recepcionera_u_hotel(user):
    clear_screen()
    print_in_mid("Dodavanje recepcionera u hotel","*")

    hoteli=control.getHotels()
    hotel=hoteli[input_by_choice(control.make_hotels_readable_dict(hoteli),"Izaberite Hotel","*")]
    clear_screen()

    korisnici=control.get_users()
    korisnik=korisnici[input_by_choice(control.readable_users_dict(korisnici),"Izaberite koga zelite za recepcionera","*")]
    clear_screen()

    control.pretvori_u_recepcionera(korisnik,hotel["id"])
    display_msg("Uspesno ste dodali recepcionera"," "," ",1)
    send_to.send_to_view(user)

def hotel_update(user):
    clear_screen()
    print_in_mid(" Admin ","*")
    tekstualni_meni={
    0 : "Izmena hotela",
    1 : "Dodavanje Sobe",
    2 : "Brisanje Sobe"
    }
    meni={
    0 : Izmena_hotela,
    1 : add_room,
    2 : delete_room
    
    }
    
    meni[input_by_choice(tekstualni_meni)](user)
    send_to.send_to_view(user)

def Izmena_hotela(user):
    hoteli=control.getHotels()
    hotel=hoteli[input_by_choice(control.make_hotels_readable_dict(hoteli),"Izaberite Hotel za Izmenu","*")]
    hotel.pop("sobe")
    clear_screen()
    while not control.check_if_edited_hotel_valid(hotel):
            hotel=fill_dict_with_str_2(hotel,["id"],[],"Hotel","*")
            clear_screen()
    control.izmeni_hotel(hotel)
    display_msg("Uspesno ste izmenili hotel"," "," ",1)
    send_to.send_to_view(user)

def add_room(user):
    hoteli=control.getHotels()
    hotel=hoteli[input_by_choice(control.make_hotels_readable_dict(hoteli),"Izaberite Hotel kojem dodajete sobu","*")]
    soba = dodavanje_sobe_u_hotel(hotel["id"])
    control.dodavanje_sobe_hotela(hotel["id"],soba)
    display_msg("Uspesno ste dodali sobu"," "," ",1)
    send_to.send_to_view(user)

def delete_room(user):
    hoteli=control.getHotels()
    hotel=hoteli[input_by_choice(control.make_hotels_readable_dict(hoteli),"Izaberite Hotel kojem brisete sobu","*")]

    sobe=dict(map(lambda soba: (soba["id"],soba),hotel["sobe"]))
    soba=sobe[input_by_choice(control.make_rooms_readable_dict(hotel["sobe"]))]
    
    s="Jeste li sigurni da zelite da obrisete sobu: "+str(soba["id"])
    if y_n_question(s):
        control.obrisi_sobu(soba)
    

    send_to.send_to_view(user)

def hotel_delete(user):
    
    hoteli=control.getHotels()
    hotel=hoteli[input_by_choice(control.make_hotels_readable_dict(hoteli),"Izaberite Hotel za Brisanje","*")]
    clear_screen()
    s="Jeste li sigurni da zelite da obrisete hotel: "+hotel["name"]
    if y_n_question(s):
        control.obrisi_hotel(hotel)



    send_to.send_to_view(user)

def brisanje_recepcionera(user):
    recepcioneri = control.get_recepcioners()
    recepcioner = recepcioneri[input_by_choice(control.readable_users_dict(recepcioneri))]
    s="Jeste li sigurni da zelite da obrisete recepcionera: "+ recepcioner["username"]
    if y_n_question(s):
        control.obrisi_recepcionera(recepcioner)
        


    send_to.send_to_view(user)

def pretraga_recepcionera(user):
    clear_screen()
    print_in_mid(" Pretraga recepcionera ","*")
    tekstualni_meni={
    0 : "Po jednom kriterijumu",
    1 : "Po vise kriterijuma"
    }
    meni={
    0 : pretraga_recepcionera_po_jednom_kriterijumu,
    1 : pretraga_recepcionera_po_vise_kriterijuma
    }
    
    meni[input_by_choice(tekstualni_meni)](user)

def pretraga_recepcionera_po_jednom_kriterijumu(user):
    kriterijumi={
        1 : "ime",
        2 : "prezime",
        3 : "username",
        4 : "email",
        5 : "hotel"
    }
    clear_screen()
    print_in_mid("Pretraga po jednom kriterijumu","*")
    izabrano = input_by_choice(kriterijumi)
    clear_screen()
    if(izabrano !=0):
        while True:
            clear_screen()
            s="Unesite "+kriterijumi[izabrano]+": "
            value = input(s)
            if(control.pretraga_recepcionera_po_jednom_kriterijumu_validate(izabrano,value)):
                break
    value = control.pretraga_recepcionera_po_jednom_kriterijumu_parse(izabrano,value)
    recepcioneri = control.pretraga_recepcionera_po_jednom_kriterijumu_get(izabrano,value)
    recepcioneri = control.readable_users_dict(dict(map(lambda recepcioner: (recepcioner["id"],recepcioner) ,recepcioneri)))
    print_dict(recepcioneri)
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to.send_to_view(user)

def pretraga_recepcionera_po_vise_kriterijuma(user):
    kriterijumi={
        "ime" : "unset",
        "prezime" : "unset",
        "username" : "unset",
        "email" : "unset",
        "hotel" : "unset"
    }
    clear_screen()
    print_in_mid("Pretraga po vise kriterijuma","*")
    while True:
        kriterijumi = fill_dict_with_str_2(kriterijumi,[],[],"Pretraga po vise kriterijuma","*")
        clear_screen()
        if control.pretraga_recepcionera_po_vise_kriterijuma_validate(kriterijumi):
            break
    kriterijumi = control.pretraga_recepcionera_po_vise_kriterijuma_parse(kriterijumi)
    recepcioners=control.pretraga_recepcionera_po_vise_kriterijuma_get(kriterijumi)
    recepcioners = control.readable_users_dict(dict(map(lambda recepcioner: (recepcioner["id"],recepcioner) ,recepcioners)))
    print_dict(recepcioners)
    input("Unesite bilo sta za povratak na prethodni meni: ")

    send_to.send_to_view(user)
