from utility import *
from controller.send_to import send_to_view
from controller.ne_registrovan_korisnik import *


current_user={}
meni={}
tekstualni_meni={}

def main():
    print_in_mid(" Neregistrovan korisnik ","*")
    meni[input_by_choice(tekstualni_meni)]

def show(user={"role":0}):
    current_user = user
    clear_screen()
    print_in_mid("Neregistrovan korisnik","*")
    meni[input_by_choice(tekstualni_meni)]()
    

def registracija():

    if y_n_question("Zelite li da se registrujete?"):
        korisnik={
            "id" : "Generisan Kasnije",
            "username" : "Not Set",
            "password" : "Not Set",
            "name" : "Not Set",
            "surname" : "Not Set",
            "phone" : "Not Set",
            "email" : "Not Set",
            "role" : "Registrovan korisnik",
            "odustani" : "n"
        }
        clear_screen()
        er=1
        korisnik = fill_dict_with_str_2(korisnik,["id","role"], [], "Registracija novog korisnika", "*")
        if korisnik["odustani"] == "n":
            er = register(korisnik)
        while(er != 0):
            clear_screen()
            korisnik = fill_dict_with_str(korisnik,["id","role"], er, "*")
            if korisnik["odustani"] == "n":
                er = register(korisnik)
        show()

    else:
        show()

    

def prijava():
    if y_n_question("Zelite li da se prijavite?"):
        creds={
            "username" : "Not Set",
            "password" : "Not Set",
        }
        clear_screen()
        creds = fill_dict_with_str(creds,[], "Prijava", "*")
        user = login(creds)
        i=0
        while((user == False) and i<3):
            clear_screen()
            creds = fill_dict_with_str(creds,[], "Username ili sifra nisu tacni", "*")
            user = login(creds)
            i+=1
        if(i==3):
            show()
        else:
            #log_something(user)
            send_to_view(user)

        
        

    else:
        show()

def pregled_hotela(user={"role" : 0}):
    current_user = user
    clear_screen()
    print_in_mid("Pregled Hotela","*")
    print("\n\n")
    for line in get_hotel_table():
        print_in_mid(line)
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to_view(current_user)

def pretraga_hotela(user={"role" : 0}):
    current_user = user
    podela={
        0 : "Po Jednom Kriterijumu",
        1 : "Vise Kriterijumska Pretraga"
    }
    poziv={
        0 : pretraga_po_jednom_kriterijumum,
        1 : vise_kriterijumska_pretraga
    }
    clear_screen()
    print_in_mid("Izbor Pretrage","*")
    poziv[input_by_choice(podela)](current_user)

    

def pretraga_po_jednom_kriterijumum(user={"role" : 0}):

    kriterijumi={
        1 : "id",
        2 : "name",
        3 : "address",
        4 : "bazen",
        5 : "restoran",
        6 : "prosecna_ocena",
        0 : "nazad"

    }
    clear_screen()
    print_in_mid("Pretraga po jednom kriterijumu","*")
    izabrano = input_by_choice(kriterijumi)
    clear_screen()
    if izabrano != 0:
        s="Unesite "+kriterijumi[izabrano]+": "
        value = input(s)
        lst=[]
        try:
            lst = get_po_jednom_kriterijumum(kriterijumi[izabrano],value)
        
        except:
            a=""
            while True:
                a = input("za kriterijume bazen i restoran moze biti samo true, false ili 1,0 ili Da,Ne")
                if(a.capitalize() in ["1","0","True","False", "Da", "Ne"]):
                    if(a.capitalize() in ["1","True","Da"]):
                        a=True
                    elif(a.capitalize() in ["0","False","Ne"]):
                        a=False
                    break
            
            value = a
            lst = get_po_jednom_kriterijumum(kriterijumi[izabrano],value)

        clear_screen()
        for line in lst:
            print_in_mid(line)
        input("Unesite bilo sta za povratak na prethodni meni: ")
        #print(user["role"])
        send_to_view(user)
    else: 
        send_to_view(user)

    

def vise_kriterijumska_pretraga(user={"role" : 0}):
    kriterijumi  = { 
    "id" : "unset", 
    "name" : "unset", 
    "address" : "unset",
    "bazen" : "unset",
    "restoran" : "unset",
    "prosecna_ocena" : "unset",

    }
    clear_screen()
    print_in_mid("Pretraga po vise kriterijuma","*")
    kriterijumi = fill_dict_with_str_2(kriterijumi,[],[],"Pretraga po vise kriterijuma; za bazen i restroan uneti true ili false")
    #print(kriterijumi)
    clear_screen()
    for line in vise_kriterijumska_pretraga_hotela(kriterijumi):
        print_in_mid(line)
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to_view(user)




def prikaz_najbolje_ocenjenih_hotela(user={"role" : 0}):
    current_user=user
    clear_screen()
    print_in_mid("Najbolje ocenjeni hoteli","*")
    for line in get_5_best():
        print_in_mid(line)
    input("Unesite bilo sta za povratak na prethodni meni: ")
    send_to_view(current_user)



meni={
    0 : registracija,
    1 : prijava,
    2 : pregled_hotela,
    3 : pretraga_hotela,
    4 : prikaz_najbolje_ocenjenih_hotela

}
tekstualni_meni={
    0 : "Registracija",
    1 : "Prijava",
    2 : "Pregled hotela",
    3 : "Pretraga hotela",
    4 : "Prikaz najbolje ocenjenih hotela"
}




