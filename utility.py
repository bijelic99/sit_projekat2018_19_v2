import os
import sys
import time
columns,lines=os.get_terminal_size()

def input_by_choice(recnik,text=" ", filler=" "):
    print_in_mid(text,filler)
    print_dict(recnik)
    key = -999
    while(True):
        try:
            key = input("Unesite sta zelite ili d za izlaz: ")
            if (key == "d" or eval(key) in recnik.keys()):
                break
        except:
            key = input("Unesite sta zelite ili d za izlaz: ")
        
    if (key == "d"):
        sys.exit()
    else:
        return eval(key)

def input_by_choice_return_multiple(recnik,text=" ",filler=" "):
    list_o_ids=[]

    print_dict(recnik)
    key = -999
    clear_screen()
    print_in_mid(text,filler)
    print_dict(recnik)

    while(True):
        try:
            key = input("Unesite sta zelite da dodate ili d za izlaz: ")
            if (key == "d"):
                break
            else:
                key=eval(key)
                list_o_ids.append(key)
                recnik.pop(key)
        except:
            break
            
        finally:
            clear_screen()
            print_in_mid(text,filler)
            print_dict(recnik)
        
    return list_o_ids

def print_dict(recnik):

    for key in recnik.keys():
        if type(key) == type("a"):
            if type(recnik[key]) == type("a"):
             s= "\n"+key+" : "+recnik[key]
            else:
                s="\n"+key+" : "+str(recnik[key])
        else:
            if type(recnik[key]) == type("a"):
             s= "\n"+str(key)+" : "+recnik[key]
            else:
                s="\n"+str(key)+" : "+str(recnik[key])
           
        s=s.center(columns)
        print_in_mid(s)

    print("\n\n")

def print_in_mid(text, filler=" "):
    text=text.center(int(columns),filler)
    print(text)

def clear_screen():
    columns,lines=os.get_terminal_size()
    os.system('cls' if os.name == 'nt' else 'clear')

def y_n_question(text):
    clear_screen()
    izbor={
        0 : "Ne",
        1 : "Da"
    }
    print_in_mid(text)
    return bool(input_by_choice(izbor))

def fill_dict_with_str(recnik, keys_to_skip=[], Naslov="", Filler=""):
    for key in recnik.keys():
        clear_screen()
        print_in_mid(Naslov,Filler)
        print("\n\n")
        print_dict(recnik)
        if(key not in keys_to_skip):
            clear_screen()
            print_in_mid(Naslov,Filler)
            print("\n\n")
            print_dict(recnik)
            while True:
                s="Unesite "+str(key)+ ": "
                x = input(s)
                if(x!=""):
                    recnik[key] = x
                    break
    return recnik

def fill_dict_with_str_2(recnik, keys_to_skip=[], required_keys=[], Naslov="", Filler=" "):
    key_dict={}
    for i,j in zip(recnik.keys(), range(len(recnik.keys()))):
        key_dict[j]=i
        if(i in required_keys):
            recnik[i]="Not_Set"
    clear_screen()
    print_in_mid(Naslov,Filler)
    print("\n\n")
    while True:
        print_in_mid("Vrednosti koje se popunjavaju")
        print_dict(recnik)
        
        print_in_mid("Broj vrednosti koju unosite")
        print_dict(key_dict)
        print("\n\n")
        a=""
        while True:
            kljuc = input("Unesite broj kljuca ili \"done\" ako ste zavrsili sa unosom: ")
            try:
                if(kljuc=="done"):
                    a="d"
                    break
                elif(eval(kljuc) in key_dict.keys() and key_dict.get(eval(kljuc), "") in keys_to_skip):
                    print("Taj kljuc ne treba popunjavati")
                    break
                elif(eval(kljuc) in key_dict.keys() and key_dict.get(eval(kljuc), "") not in keys_to_skip):
                    s = "Unesite "+str(key_dict.get(eval(kljuc), ""))+": "
                    s = input(s)
                    recnik[key_dict[eval(kljuc)]]=s
                    break
            except:
                print("Greska pri unosu")
            
        if(a == "d"):
            a = True
            for key in recnik.keys():
                if(recnik[key]=="Not_Set"):
                    a = False
            if(not a):
                clear_screen()
                print_in_mid(Naslov,Filler)
                print_in_mid("Nisu popunjene sve zahtevane vrednosti")
            else:
                break
        else:
            clear_screen()
            print_in_mid(Naslov,Filler)
            print("\n\n")

    return recnik
    
def display_msg(msg1=" ",msg2=" ",filler=" ",time_s=1):
    clear_screen()
    print_in_mid(msg1,filler)
    print_in_mid(msg2,filler)
    time.sleep(time_s)
    clear_screen()


def log_something(text,path="out.txt"):
    try:
        doc = open(path,"a")
        s=""
        if(type(text)==type("a")):
            s=text+"\n"
        else:
            s=str(text)+"\n"
        doc.write(s)

        doc.close()
    except:
        return

