import model.soba as soba
import model.rezervacija
import model.korisnik
import utility

from datetime import *


def pretraga_soba_po_jednom_kriterijumu_validate(kriterijum, vrednost):

    kriterijumi={
        1 : "room_number",
        2 : "no_of_beds",
        3 : "type",
        4 : "ac",
        5 : "tv",
        6 : "balcony",
        7 : "bathroom",
        8 : "price_per_night"
    }

    good = False
    if(kriterijum in kriterijumi.keys()):
        if kriterijum in [1,2,8]:
            try:
                if(type(eval(vrednost)) == type(1)):
                    good = True
            except:
                good = False
        elif kriterijum == 3:
            try:
                if(vrednost in soba.room_type.values() or (type(eval(vrednost)) == type(1) and eval(vrednost) in soba.room_type.keys())):
                    good = True
            except:
                good = False
        elif kriterijum in [4,5,6]:
            phrases=["Da","1","True","Ne","0","False"]
            if vrednost.capitalize() in phrases:
                good = True
            else:
                good=False
        elif kriterijum == 7:
            try:
                if(vrednost in soba.kupatilo.values() or (type(eval(vrednost)) == type(1) and eval(vrednost) in soba.kupatilo.keys())):
                    good = True
            except:
                good = False
        



    return good

def parse_value_room(kriterijum,vrednost):
    if(kriterijum in ["room_number","no_of_beds","price_per_night"]):
        vrednost = eval(vrednost)

    elif(kriterijum == "type"):
        if(vrednost in soba.room_type.values()):
            vrednost=list(filter(lambda x: x[1]==vrednost,soba.room_type.items()))[0][0]
        else:
            vrednost = eval(vrednost)

    elif(kriterijum in ["ac","tv","balcony"]):
        good_phrases=["Da","1","True"]
        bad_phrases =["Ne","0","False"]
        if(vrednost.capitalize() in good_phrases):
            vrednost = True
        elif(vrednost.capitalize() in bad_phrases):
            vrednost = False

    elif(kriterijum == "bathroom"):
        if(vrednost in soba.kupatilo.values()):
            vrednost=list(filter(lambda x: x[1]==vrednost,soba.kupatilo.items()))[0][0]
        else:
            vrednost = eval(vrednost)
    return vrednost


def get_soba_jedan_kriterijum(hotel_id,kriterijum,vrednost):
    vrednost = parse_value_room(kriterijum,vrednost)
    sobe = []
    sobe=list(filter(lambda soba1: soba1["hotel_id"]==hotel_id and soba1[kriterijum]==vrednost, soba.sve_sobe.values()))
    return sobe

def format_sobe_for_print(sobe):
    formated = []
    formated = list(map(lambda soba1: format_soba(soba1),sobe))
    #utility.log_something(formated)
    formated.insert(0,soba_header())

    return formated
def soba_header():
    s=""
    s+="id".center(10)
    s+="hotel_id".center(10)
    s+="broj_sobe".center(10)
    s+="broj_kreveta".center(10)
    s+="tip_sobe".center(12)
    s+="klima".center(10)
    s+="tv".center(10)
    s+="balkon".center(10)
    s+="kupatilo".center(12)
    s+="cena".center(12)
    return s
def format_soba(soba1):
    s=""
    s+=str(soba1["id"]).center(10)
    s+=str(soba1["hotel_id"]).center(10)
    s+=str(soba1["room_number"]).center(10)
    s+=str(soba1["no_of_beds"]).center(10)
    s+=(soba.room_type[soba1["type"]]).center(12)
    s+=str(soba1["ac"]).center(10)
    s+=str(soba1["tv"]).center(10)
    s+=str(soba1["balcony"]).center(10)
    s+=(soba.kupatilo[soba1["bathroom"]]).center(12)
    s+=str(soba1["price_per_night"]).center(12)
    return s

def datumi_za_pretragu_soba_validate(datum_od,datum_do):
    good = False
    try:
        datum_od = make_date(datum_od)
    except:
        return False
    try:
        datum_do = make_date(datum_do)
    except:
        return False
    if (datum_do-datum_od).days > 0:
        good = True
    return good
    

    

def datumi_za_pretragu_soba_parse(datum_od,datum_do):
    return make_date(datum_od),make_date(datum_do)

def make_date(str_date):
    date=str_date.split("-")
    given_date = datetime(eval(date[2].lstrip("0")),eval(date[1].lstrip("0")),eval(date[0].lstrip("0")))
    return given_date
def get_soba_slobodnih_za_datume(hotel_id, datum_od, datum_do):
    sobe=[]
    zauzete_sobe=[]
    rezervacije = dict(filter(lambda rezervacija: rezervacija[1]["hotel_id"]==hotel_id and (not (rezervacija[1]["datum_odjave"]<=datum_od or rezervacija[1]["datum_prijave"]>=datum_do)), model.rezervacija.sve_rezervacije.items()))
    #utility.log_something(rezervacije)
    for rezervacija in rezervacije.values():
        for soba1 in rezervacija["rezervisane_sobe"]:
            zauzete_sobe.append(soba1)
    #utility.log_something(zauzete_sobe)
    sobe = list(filter(lambda soba1: soba1["hotel_id"]==hotel_id and soba1 not in zauzete_sobe, model.soba.sve_sobe.values()))
    return sobe

def vise_kriterijumska_pretraga_soba_validate(kriterijumi):
    
    for jedan_kriterijum in kriterijumi.items():
        if not jedan_kriterijum[1]=="unset":
            if jedan_kriterijum[0] in ["room_number","no_of_beds","price_per_night"]:
                try:
                    if(type(eval(jedan_kriterijum[1]))==type(1)):
                        continue
                except:
                    return False
            elif jedan_kriterijum[0] == "type":
                try:
                    if(jedan_kriterijum[1] in soba.room_type.values()):
                        continue
                    elif(type(eval(jedan_kriterijum[1]))==type(1)):
                        if(eval(jedan_kriterijum) in soba.room_type.keys()):
                            continue
                        else:
                            raise Exception("Wrong number m8")
                except:
                    return False
            elif jedan_kriterijum[0] in ["ac","tv","balcony"]:
                phrases=["Da","1","True","Ne","0","False"]
                try:
                    if jedan_kriterijum[1].capitalize() in phrases:
                        continue
                    else:
                        raise Exception("Wrong number m8")
                except:
                    return False
            elif jedan_kriterijum[0] == "bathroom":
                try:
                    if(jedan_kriterijum[1] in soba.kupatilo.values()):
                        continue
                    elif(type(eval(jedan_kriterijum[1]))==type(1)):
                        if(eval(jedan_kriterijum) in soba.kupatilo.keys()):
                            continue
                        else:
                            raise Exception("Wrong number m8")
                except:
                    return False

    return True

def vise_kriterijumska_pretraga_soba_parse(kriterijumi):
    
    for kljuc in kriterijumi.keys():
        if not kriterijumi[kljuc]=="unset":
            if kljuc in ["room_number","no_of_beds","price_per_night"]:
                kriterijumi[kljuc] = eval(kriterijumi[kljuc])
            elif kljuc == "type":
                if kriterijumi[kljuc] in soba.room_type.values():
                    kriterijumi[kljuc]=list(filter(lambda x: x[1]==kriterijumi[kljuc],soba.room_type.items()))[0][0]
                else:
                    kriterijumi[kljuc] = eval(kriterijumi[kljuc])
            elif kljuc in ["ac","tv","balcony"]:
                good_phrases=["Da","1","True"]
                bad_phrases =["Ne","0","False"]
                if(kriterijumi[kljuc].capitalize() in good_phrases):
                    kriterijumi[kljuc] = True
                elif(kriterijumi[kljuc].capitalize() in bad_phrases):
                    kriterijumi[kljuc] = False
            elif kljuc == "bathroom":
                if kriterijumi[kljuc] in soba.kupatilo.values():
                    kriterijumi[kljuc]=list(filter(lambda x: x[1]==kriterijumi[kljuc],soba.kupatilo.items()))[0][0]
                else:
                    kriterijumi[kljuc] = eval(kriterijumi[kljuc])

    return kriterijumi

def get_soba_vise_kriterijum(hotel_id,kriterijumi,sobe=list(soba.sve_sobe.values())):
    for key in kriterijumi.keys():
        if kriterijumi[key] != "unset":
            sobe=list(filter(lambda soba1: soba1["hotel_id"]==hotel_id and soba1[key] == kriterijumi[key],sobe))

    return sobe

def get_rezervacije_za_period(user,period):
    rez_dict={}
    tday = datetime.now()
    #dnevni
    if period == 0:
        rez_dict = dict(filter(lambda rez: rez[1]["hotel_id"]==user["hotel_id"] and rez[1]["datum_i_vreme_rezervacije"].date()==tday.date(),model.rezervacija.sve_rezervacije.items()))
    #nedeljni
    if period == 1:
        day_of_week = tday.weekday()
        till_end=timedelta(days=(6-day_of_week))
        from_start=timedelta(days=(day_of_week))
        end_of_week_date=tday+till_end
        start_of_week_date=tday-from_start
        #utility.log_something(start_of_week_date)
        #utility.log_something(end_of_week_date)
        rez_dict = dict(filter(lambda rez: rez[1]["hotel_id"]==user["hotel_id"] and (rez[1]["datum_i_vreme_rezervacije"].date()-start_of_week_date.date()).days > 0 and (rez[1]["datum_i_vreme_rezervacije"].date()-end_of_week_date.date()).days < 0, model.rezervacija.sve_rezervacije.items()))
    #mesecni
    if period == 2:
        day_of_mounth = tday.day
        from_start = timedelta(days=(day_of_mounth))
        month_start_date = tday-from_start
        month_end_date = month_start_date+timedelta(days=29)
        rez_dict = dict(filter(lambda rez: rez[1]["hotel_id"]==user["hotel_id"] and (rez[1]["datum_i_vreme_rezervacije"]-month_start_date).days > 0 and (rez[1]["datum_i_vreme_rezervacije"]-month_end_date).days < 0, model.rezervacija.sve_rezervacije.items()))
    
    return rez_dict

def get_broj_realizovanih_rezervacija(user,period):
    return len(get_rezervacije_za_period(user,period).keys())

def get_broj_izdatih_soba(user,period):
    i = 0
    for rez in get_rezervacije_za_period(user,period).values():
        i+=len(rez["rezervisane_sobe"])
    return i

def get_zarada(user,period):
    i = 0
    for rez in get_rezervacije_za_period(user,period).values():
        dana = (rez["datum_odjave"]-rez["datum_prijave"]).days
        for soba1 in rez["rezervisane_sobe"]:
            i+=soba1["price_per_night"]*dana
    return i

def get_prosecna_ocena(user,period):
    try:
        i=0
        j=0
        for rez in get_rezervacije_za_period(user,period).values():
            if(rez["ocena_hotela"] != 0):
                j+=1
                i+=rez["ocena_hotela"]
        return i/j
    except:
        return 0
"""
kriterijumi za pretragu rezervacija kod recepcionera

kriterijumi={
        1 : "Datum kreacije",
        2 : "Datum prijave",
        3 : "Datum odjave",
        4 : "Korisnik koji je kreirao",
        5 : "status rezervacije"

    }

"""
def pretraga_rezervacija_po_jednom_kriterijumu_validate(izabrano,value):
    if izabrano in [1,2,3]:
        try:
            value=value.split("-")
            value[0]=eval(value[0])
            value[1]=eval(value[1])
            value[2]=eval(value[2])
            date = datetime(value[2],value[1],value[0])
        except:
            return False
    elif izabrano == 4:
        
        try:
            if value in list(map(lambda user:  user["username"], model.korisnik.svi_korisnici.values())):
                return True
            elif eval(value) in list(map(lambda user:  user["id"], model.korisnik.svi_korisnici.values())):
                return True
        except:
            return False
    elif izabrano == 5:
        try:
            if value in model.rezervacija.statusi_rezervacije.values():
                return True
            elif eval(value) in model.rezervacija.statusi_rezervacije.keys():
                return True
        except:
            return False


    return True

def pretraga_rezervacija_po_jednom_kriterijumu_parse(izabrano,value):
    if izabrano in [1,2,3]:
        value=value.split("-")
        value[0]=eval(value[0])
        value[1]=eval(value[1])
        value[2]=eval(value[2])
        value = datetime(value[2],value[1],value[0])
    elif izabrano == 4:
        if value in list(map(lambda user:  user["username"], model.korisnik.svi_korisnici.values())):
            return value
        elif eval(value) in list(map(lambda user:  user["id"], model.korisnik.svi_korisnici.values())):
            return list(filter(lambda kljuc,osoba: kljuc==eval(value),model.korisnik.svi_korisnici.items()))[0][1]["username"]
    elif izabrano == 5:
        if value in model.rezervacija.statusi_rezervacije.values():
            return list(filter(lambda kljuc,status: status==value, model.rezervacija.statusi_rezervacije.items()))[0][0]
        elif eval(value) in model.rezervacija.statusi_rezervacije.keys():
            return eval(value)
        

    return value

def get_pretraga_rezervacija_po_jednom_kriterijumu(hotel_id,izabrano,value):
    rezervacije_dict = {}
    if izabrano == 1:
        rezervacije_dict = dict(filter(lambda rezervacija: rezervacija[1]["hotel_id"] == hotel_id and rezervacija[1]["datum_i_vreme_rezervacije"].date() == value.date(),model.rezervacija.sve_rezervacije.items()))
    elif izabrano == 2:
        rezervacije_dict = dict(filter(lambda rezervacija: rezervacija[1]["hotel_id"] == hotel_id and rezervacija[1]["datum_prijave"].date() == value.date(),model.rezervacija.sve_rezervacije.items()))
    elif izabrano == 3:
        rezervacije_dict = dict(filter(lambda rezervacija: rezervacija[1]["hotel_id"] == hotel_id and rezervacija[1]["datum_odjave"].date() == value.date(),model.rezervacija.sve_rezervacije.items()))
    elif izabrano == 4:
        rezervacije_dict = dict(filter(lambda rezervacija: rezervacija[1]["hotel_id"] == hotel_id and rezervacija[1]["username_korisnika"] == value,model.rezervacija.sve_rezervacije.items()))
    elif izabrano == 5:
        rezervacije_dict = dict(filter(lambda rezervacija: rezervacija[1]["hotel_id"] == hotel_id and rezervacija[1]["status_rezervacije"] == value,model.rezervacija.sve_rezervacije.items()))
    
    return rezervacije_dict

def vise_kriterijumska_pretraga_rezervacija_validate(kriterijumi):
    for key in kriterijumi.keys():
        if (kriterijumi[key] != "unset"):
            if key in ["Datum kreacije", "Datum prijave", "Datum odjave"]:
                try:
                    value = kriterijumi[key]
                    value = value.split("-")
                    value[0], value[1], value[2]=eval(value[0]), eval(value[1]), eval(value[2])
                    date = datetime(value[2],value[1], value[0])
                except:
                    return False
            if key == "Korisnik koji je kreirao":
                try:
                    if kriterijumi[key] not in list(map(lambda korisnik: korisnik["username"],model.korisnik.svi_korisnici.values())):
                        if eval(kriterijumi[key]) not in list(map(lambda korisnik: korisnik["id"],model.korisnik.svi_korisnici.values())):
                            raise Exception("User not found")
                except:
                    return False
            if key == "Status rezervacije":
                try:
                    if kriterijumi[key] not in list(model.rezervacija.statusi_rezervacije.values()):
                        if eval(kriterijumi[key]) not in list(model.rezervacija.statusi_rezervacije.keys()):
                            raise Exception("User not found")
                except:
                    return False


    return True

def vise_kriterijumska_pretraga_rezervacija_parse(kriterijumi):
    for key in kriterijumi.keys():
        if (kriterijumi[key] != "unset"):
            if key in ["Datum kreacije", "Datum prijave", "Datum odjave"]:
                value = kriterijumi[key]
                value = value.split("-")
                value[0], value[1], value[2]=eval(value[0]), eval(value[1]), eval(value[2])
                date = datetime(value[2],value[1], value[0])
                #utility.log_something(date)
                kriterijumi[key]=date
            if key == "Korisnik koji je kreirao":
                if kriterijumi[key] in list(map(lambda korisnik: korisnik["username"],model.korisnik.svi_korisnici.values())):
                    continue
                elif eval(kriterijumi[key]) in list(map(lambda korisnik: korisnik["id"],model.korisnik.svi_korisnici.values())):
                    kriterijumi[key] = model.korisnik.svi_korisnici(eval(kriterijumi[key]))["username"]
            if key == "Status rezervacije":
                if kriterijumi[key] in list(model.rezervacija.statusi_rezervacije.values()):
                    kriterijumi[key] = list(filter(lambda x: x[1]==kriterijumi[key],model.rezervacija.statusi_rezervacije.items()))[0][0]
                else:
                    kriterijumi[key] = eval(kriterijumi[key])

    return kriterijumi

def get_vise_kriterijumska_pretraga_rezervacija(hotel_id,kriterijumi):
    filtered = list(filter(lambda res: res["hotel_id"] == hotel_id, model.rezervacija.sve_rezervacije.values()))
    #utility.log_something(kriterijumi)
    for key in kriterijumi.keys():
        if kriterijumi[key] != "unset":
            if(key == "Datum kreacije"):
                filtered = list(filter(lambda res: res["datum_i_vreme_rezervacije"].date() == kriterijumi[key].date(), filtered))
            if(key == "Datum prijave"):
                filtered = list(filter(lambda res: res["datum_prijave"].date() == kriterijumi[key].date(), filtered))
            if(key == "Datum odjave"):
                filtered = list(filter(lambda res: res["datum_odjave"].date() == kriterijumi[key].date(), filtered))
            if(key == "Korisnik koji je kreirao"):
                filtered = list(filter(lambda res: res["username_korisnika"] == kriterijumi[key], filtered))
            if(key == "Status rezervacije"):
                filtered = list(filter(lambda res: res["status_rezervacije"] == kriterijumi[key], filtered))
            
            
    return filtered