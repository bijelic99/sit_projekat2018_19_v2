import model.rezervacija
import model.hotel
import model.soba
from controller.registrovan_korisnik import getHotels,make_hotels_readable_dict,make_rooms_readable_dict
import copy
import utility


def check_if_new_hotel_is_valid(hotel):
    """
    hotel  = { 
    "id" : "", 
    "name" : "", 
    "address" : "",
    "bazen" : "",
    "restoran" : "",
    "prosecna_ocena" : ""
    }
    """
    good=False
    try:
        
        good_phrases = ["1","0","Da","Ne","Yes","No","True","False"]
        if(hotel["bazen"].capitalize() in good_phrases and hotel["restoran"].capitalize() in good_phrases):
            good=True
        else:
            raise Exception("Not Good")
        a=eval(hotel["prosecna_ocena"])
        if(type(a) in [type(5),type(1.25)]):
            good=True
        else:
            raise Exception("Not Good")
        
    except:
        return False
    return good
def check_if_edited_hotel_valid(hotel):
    good=False
    try:
        
        good_phrases = ["1","0","Da","Ne","Yes","No","True","False"]
        if(type(hotel["bazen"])==type("a")):
            if(hotel["bazen"].capitalize() in good_phrases):
                good=True
            else:
                raise Exception("Not Good")
        if(type(hotel["restoran"])==type("a")):
            if(hotel["restoran"].capitalize() in good_phrases):
                good=True
            else:
                raise Exception("Not Good")
        if(type(hotel["prosecna_ocena"]) not in[type(2),type(2.5)]):
            a=eval(hotel["prosecna_ocena"])
            if(type(a) in [type(5),type(1.25)]):
                good=True
            else:
                raise Exception("Not Good")
        
    except:
        return False
    return good
def check_if_new_room_is_valid(room):
    """
    soba={
    "id":"0",
    "hotel_id":"0",
    "room_number":"0",
    "no_of_beds":"1",
    "type":"0",
    "ac":"0",
    "tv":"0",
    "balcony":"0",
    "bathroom":"0",
    "price_per_night":"0"
    }
    """
    good = False
    try:
        if(eval(room["room_number"])>0):
            good = True
        else:
            raise Exception("Not Good")
        if(eval(room["no_of_beds"])>0):
            good = True
        else:
            raise Exception("Not Good")
        if(eval(room["type"]) in [0,1]):
            good = True
        else:
            raise Exception("Not Good")
        good_phrases = ["1","0","Da","Ne","Yes","No","True","False"]
        if(room["ac"].capitalize() in good_phrases and room["tv"].capitalize() in good_phrases and room["balcony"].capitalize() in good_phrases):
            good = True
        else:
            raise Exception("Not Good")
        if(eval(room["bathroom"]) in [0,1]):
            good = True
        else:
            raise Exception("Not Good")
        a=eval(room["price_per_night"])
        if(type(a) == type(5)):
            good=True
        else:
            raise Exception("Not Good")
    except:
        return False
    return good

def add_hotel(hotel):
    hotel["id"]=model.hotel.generate_id()

    true_phrases=["Da","1","True","Yes"]
    false_phrases=["Ne","0","False","No"]

    if(hotel["bazen"].capitalize() in true_phrases):
        hotel["bazen"]=True
    elif(hotel["bazen"].capitalize() in false_phrases):
        hotel["bazen"]=False
    else:
        hotel["bazen"]=False

    if(hotel["restoran"].capitalize() in true_phrases):
        hotel["restoran"]=True
    elif(hotel["restoran"].capitalize() in false_phrases):
        hotel["restoran"]=False
    else:
        hotel["restoran"]=False
    hotel["prosecna_ocena"]=eval(hotel["prosecna_ocena"])
    model.hotel.dodaj_hotel(hotel)
    return hotel

def dodavanje_soba_hotela(hotel_id,sobe):
    #map primenjuje na svaku
    sobe=list(map(lambda room: parse_room(room),sobe))
    model.hotel.svi_hoteli[hotel_id]["sobe"]=sobe
    for soba in sobe:
        model.soba.dodaj_sobu(soba)

    return False

def dodavanje_sobe_hotela(hotel_id,soba):
    #map primenjuje na svaku
    soba = parse_room(soba)
    model.hotel.svi_hoteli[hotel_id]["sobe"].append(soba)
    model.soba.dodaj_sobu(soba)

    return False

def parse_room(room):
    room["hotel_id"]=eval(room["hotel_id"])
    room["room_number"]=eval(room["room_number"])
    room["no_of_beds"]=eval(room["no_of_beds"])
    room["type"]=eval(room["type"])

    true_phrases=["Da","1","True","Yes",1]
    false_phrases=["Ne","0","False","No",0]

    if(room["ac"] in true_phrases):
        room["ac"]=True
    elif(room["ac"] in false_phrases):
        room["ac"]=False
    else:
        room["ac"]=False

    if(room["tv"] in true_phrases):
        room["tv"]=True
    elif(room["tv"] in false_phrases):
        room["tv"]=False
    else:
        room["tv"]=False
    
    if(room["balcony"] in true_phrases):
        room["balcony"]=True
    elif(room["balcony"] in false_phrases):
        room["balcony"]=False
    else:
        room["balcony"]=False
    
    room["bathroom"]=eval(room["bathroom"])
    room["price_per_night"]=eval(room["price_per_night"])

    return room

def get_users():
    return model.korisnik.svi_korisnici

def readable_users_dict(korisnici):
    str_korisnici={}
    str_korisnici["ID"]=korisnik_keys_table_heder()
    for key in korisnici.keys():
        str_korisnici[key]=korisnik_to_str(korisnici[key])
    return str_korisnici

def korisnik_keys_table_heder():
    s=""
    s+="id".center(10)
    s+="username".center(20)
    s+="name".center(20)
    s+="surname".center(20)
    s+="phone".center(20)
    s+="email".center(20)
    s+="role".center(20)

    return s

def korisnik_to_str(korisnik):
    s=""
    s+=str(korisnik["id"]).center(10)
    s+=korisnik["username"].center(20)
    s+=korisnik["name"].center(20)
    s+=korisnik["surname"].center(20)
    s+=korisnik["phone"].center(20)
    s+=korisnik["email"].center(20)
    s+=model.korisnik.roles[korisnik["role"]].center(20)

    return s

def pretvori_u_recepcionera(user,hotel_id):
    user["role"]=2
    user["hotel_id"]=hotel_id
    model.korisnik.izmeni_korisnika(user)

    return False

def obrisi_hotel(hotel):
    if(hotel["id"] in model.hotel.svi_hoteli.keys()):
        #keys = copy.deepcopy()
        for key in list(model.soba.sve_sobe.keys()):
            if model.soba.sve_sobe[key]["hotel_id"]==hotel["id"]:
                model.soba.obrisi_sobu(key)
        model.hotel.obrisi_hotel(hotel["id"])

def get_recepcioners():
    return dict(filter(lambda x: x[1]["role"]==2 ,get_users().items()))

    #utility.log_something(recepcioneri)

def obrisi_recepcionera(recepcioner):
    recepcioner["role"]=1
    model.korisnik.izmeni_korisnika(recepcioner)
    model.korisnik.obrisi_Recepcioner_hotel(recepcioner["id"])
    

def izmeni_hotel(hotel):
    hotel["sobe"]=model.hotel.svi_hoteli[hotel["id"]]["sobe"]
    true_phrases=["Da","1","True","Yes"]
    false_phrases=["Ne","0","False","No"]
    if(type(hotel["bazen"])==type("a")):
        if(hotel["bazen"].capitalize() in true_phrases):
            hotel["bazen"]=True
        elif(hotel["bazen"].capitalize() in false_phrases):
            hotel["bazen"]=False
        else:
            hotel["bazen"]=False
    if(type(hotel["restoran"])==type("a")):
        if(hotel["restoran"].capitalize() in true_phrases):
            hotel["restoran"]=True
        elif(hotel["restoran"].capitalize() in false_phrases):
            hotel["restoran"]=False
        else:
            hotel["restoran"]=False
    if(type(hotel["prosecna_ocena"])==type("a")):
        hotel["prosecna_ocena"]=eval(hotel["prosecna_ocena"])
    model.hotel.izmeni_hotel(hotel)

def obrisi_sobu(soba):

    rez_sa_sobom=dict(filter(lambda rezervacija: soba in rezervacija[1]["rezervisane_sobe"],model.rezervacija.sve_rezervacije.items()))

    if(len(rez_sa_sobom.keys())!=0):
        for key in rez_sa_sobom.keys():
            model.rezervacija.obrisi_rezervaciju(key)
            
    model.soba.obrisi_sobu(soba["id"])


def pretraga_recepcionera_po_jednom_kriterijumu_validate(izabrano,value):
    """
    kriterijumi={
        1 : "ime",
        2 : "prezime",
        3 : "username",
        4 : "email",
        5 : "hotel"
    }
    """
    recepcioneri = list(filter(lambda osb: osb["role"]==2, model.korisnik.svi_korisnici.values()))
    
    if izabrano == 5:
        try:
            value= eval(value)
            if value not in list(map(lambda recepcioner : recepcioner["hotel_id"], recepcioneri)):
                return False
        except:
            hoteli = list(model.hotel.svi_hoteli.values())
            if value not in list(map(lambda hotel : hotel["name"], hoteli)):
                return False
    return True

def pretraga_recepcionera_po_jednom_kriterijumu_parse(izabrano,value):

    if izabrano == 5:    
        try:
                value = eval(value)
        except:
                hoteli = list(model.hotel.svi_hoteli.values())
                value = list(filter(lambda hotel : hotel["name"]==value,hoteli))[0]["id"]

    return value

def pretraga_recepcionera_po_jednom_kriterijumu_get(izabrano,value):
    recepcioneri = list(filter(lambda osb: osb["role"]==2, model.korisnik.svi_korisnici.values()))
    if izabrano == 1:
        recepcioneri = list(filter(lambda osb: osb["name"]==value,recepcioneri))
    elif izabrano == 2:
        recepcioneri = list(filter(lambda osb: osb["surname"]==value,recepcioneri))
    elif izabrano == 3:
        recepcioneri = list(filter(lambda osb: osb["username"]==value,recepcioneri))
    elif izabrano == 4:
        recepcioneri = list(filter(lambda osb: osb["email"]==value,recepcioneri))
    elif izabrano == 5:
        recepcioneri = list(filter(lambda osb: osb["hotel_id"]==value,recepcioneri))
    return recepcioneri

def pretraga_recepcionera_po_vise_kriterijuma_validate(kriterijumi):
    recepcioneri = list(filter(lambda osb: osb["role"]==2, model.korisnik.svi_korisnici.values()))
    for key in kriterijumi.keys():
        if kriterijumi[key] != "unset":
            
            if key == "hotel":
                try:
                    value = eval(kriterijumi[key])
                    if value not in map(lambda hotel: hotel["id"], model.hotel.svi_hoteli.values()):
                        return False
                except:
                    if kriterijumi[key] not in map(lambda hotel: hotel["name"], model.hotel.svi_hoteli.values()):
                        return False
    return True

def pretraga_recepcionera_po_vise_kriterijuma_parse(kriterijumi):

    for key in kriterijumi.keys():
        if kriterijumi[key] != "unset":
            if key == "hotel":
                try:
                    kriterijumi[key] = eval(kriterijumi[key])
                    
                except:
                    kriterijumi[key] = list(filter(lambda hot: hot["name"]==kriterijumi[key],model.hotel.svi_hoteli.values()))[0]["id"]
    

    return kriterijumi

def pretraga_recepcionera_po_vise_kriterijuma_get(kriterijumi):
    recepcioneri = list(filter(lambda osb: osb["role"]==2, model.korisnik.svi_korisnici.values()))
    for key in kriterijumi.keys():
        if kriterijumi[key] != "unset":
            if key == "ime":
                recepcioneri = list(filter(lambda recep: recep["name"] == kriterijumi[key], recepcioneri))
            elif key == "prezime":
                recepcioneri = list(filter(lambda recep: recep["surname"] == kriterijumi[key], recepcioneri))
            elif key == "username":
                recepcioneri = list(filter(lambda recep: recep["username"] == kriterijumi[key], recepcioneri))
            elif key == "email":
                recepcioneri = list(filter(lambda recep: recep["email"] == kriterijumi[key], recepcioneri))
            elif key == "hotel":
                recepcioneri = list(filter(lambda recep: recep["hotel_id"] == kriterijumi[key], recepcioneri))

    return recepcioneri