import model.korisnik as korisnik
import model.hotel as hotel


def register(user):
    error = 0
    users = korisnik.svi_korisnici
    for key in users.keys():
        
        if(user["username"]==users[key]["username"] or user["username"]=="Not Set" or user["username"]=="" or user["username"]==" "):
            error = "Vec postoji korisnik sa istim username molimo unesite drugi username ili niste uneli username"
        if(user["password"] in ["Not Set",""," "]):
            error = "Morate uneti sifru"
        
    for key in user.keys():
        if(key not in ["username","password","odustani"]):
            if user[key] in ["Not Set",""," "]:
                error = "Morate popuniti " + key
    if(error == 0):
        user["role"]=1
        korisnik.dodaj_korisnika(user)

    return error

def login(creds):
    user = korisnik.get_user(creds["username"],creds["password"])
    return user

def get_hotel_table(hoteli = hotel.svi_hoteli):
    table=[]
    #s="id\time\tadresa\tbazen\trestoran\tprosecna ocena"
    s="Id".center(10)+"Ime".center(20)+"Adresa".center(40)+"Bazen".center(10)+"Restoran".center(10)+"Prosecna Ocena".center(20)
    table.append(s)
    for key in  hoteli.keys():
        s=""
        """
        for atr in hoteli[key].keys():
            s+=str(atr)+"\tb"
        """
        #s=str(hoteli[key]["id"]).center(10)+"\t"+hoteli[key]["name"].center(20)+"\t"+hoteli[key]["address"].center(20)+"\t"+str(hoteli[key]["bazen"]).center(10)+"\t"+str(hoteli[key]["restoran"]).center(10)+"\t"+str(hoteli[key]["prosecna_ocena"]).center(10)
        s=str(hoteli[key]["id"]).center(10)+hoteli[key]["name"].center(20)+hoteli[key]["address"].center(40)+str(hoteli[key]["bazen"]).center(10)+str(hoteli[key]["restoran"]).center(10)+str(hoteli[key]["prosecna_ocena"]).center(20)
        table.append(s)

    return table

def get_po_jednom_kriterijumum(kriterijum, vrednost):
    hoteli={}
    if(kriterijum in ["id", "prosecna_ocena"]):
        vrednost = eval(vrednost)
    elif(kriterijum in ["bazen", "restoran"]):
        
        if(type(vrednost) == type("a") and vrednost.capitalize() in ["1","0","True","False", "Da", "Ne"]):
                    if(vrednost.capitalize() in ["1","True","Da"]):
                        vrednost=True
                    elif(vrednost.capitalize() in ["0","False","Ne"]):
                        vrednost=False
        elif(type(vrednost) == type(True)):
            vrednost=vrednost
        else:
            print(type(vrednost),vrednost)
            raise ValueError("Not Good Bool")
        
    for key in hotel.svi_hoteli.keys():
        if hotel.svi_hoteli[key][kriterijum] == vrednost:
            hoteli[hotel.svi_hoteli[key]["id"]]=hotel.svi_hoteli[key]
    return get_hotel_table(hoteli)

def get_5_best():
    lst=[]
    lst = list(sorted(hotel.svi_hoteli.values(),key = lambda hotel2: hotel2["prosecna_ocena"], reverse=True))
    hoteli={}
    for htl in lst[:5]:
        hoteli[htl["id"]]=htl
    return get_hotel_table(hoteli)
    





"""
kriterijumi  = { 
    "id" : "unset", 
    "name" : "unset", 
    "address" : "unset",
    "bazen" : "unset",
    "restoran" : "unset",
    "prosecna_ocena" : "unset",

    }
"""



def vise_kriterijumska_pretraga_hotela(kriterijumi):
    hoteli = hotel.svi_hoteli
    hoteli2 = {}
    i=0
    for key in kriterijumi.keys():
        if(kriterijumi[key]=="unset"):
            i+=1
    if(i==6):
        return get_hotel_table(hoteli)
    else:
        for hotel_key in hoteli.keys():
            for kriterijum_key in kriterijumi.keys():
                if(kriterijumi[kriterijum_key] != "unset"):
                    if(kriterijum_key == "id" and hoteli[hotel_key]["id"] == eval(kriterijumi["id"])):
                        hoteli2[hotel_key]=hoteli[hotel_key]
                    
                    if(kriterijum_key == "name" and hoteli[hotel_key]["name"] == kriterijumi["name"]):
                        hoteli2[hotel_key]=hoteli[hotel_key]
                    
                    if(kriterijum_key == "address" and hoteli[hotel_key]["address"] == kriterijumi["address"]):
                        hoteli2[hotel_key]=hoteli[hotel_key]
                    
                    if((kriterijum_key == "bazen" or kriterijum_key == "restoran") and type(kriterijumi[kriterijum_key]) != type(True)):
                        if(kriterijumi[kriterijum_key].capitalize() in ["True","1","Da"]):
                            kriterijumi[kriterijum_key]=True
                        elif(kriterijumi[kriterijum_key].capitalize() in ["False","0","Ne"]):
                            kriterijumi[kriterijum_key]=False
                        else:
                            kriterijumi[kriterijum_key]=False
                    if(kriterijum_key == "bazen" and hoteli[hotel_key]["bazen"] == kriterijumi["bazen"]):
                        hoteli2[hotel_key]=hoteli[hotel_key]
                    
                    if(kriterijum_key == "restoran" and hoteli[hotel_key]["restoran"] == kriterijumi["restoran"]):
                        hoteli2[hotel_key]=hoteli[hotel_key]
                    
                    if(kriterijum_key == "prosecna_ocena" and hoteli[hotel_key]["prosecna_ocena"] == eval(kriterijumi["prosecna_ocena"])):
                        hoteli2[hotel_key]=hoteli[hotel_key]
    # nije moguce brisati iz recnika dok se prolazi kroz isti!!!

        for hotel_key in list(hoteli2.keys()):
            for kriterijum_key in kriterijumi.keys():
                if(kriterijumi[kriterijum_key] != "unset"):
                    if(hotel_key in hoteli2.keys() and kriterijum_key == "id" and hoteli2[hotel_key]["id"] != eval(kriterijumi["id"])):
                        hoteli2.pop(hotel_key)
                        
                    
                    if(hotel_key in hoteli2.keys() and kriterijum_key == "name" and hoteli2[hotel_key]["name"] != kriterijumi["name"]):
                        hoteli2.pop(hotel_key)
                        
                    
                    if(hotel_key in hoteli2.keys() and kriterijum_key == "address" and hoteli2[hotel_key]["address"] != kriterijumi["address"]):
                        hoteli2.pop(hotel_key)
                        
                    
                    if(hotel_key in hoteli2.keys() and kriterijum_key == "bazen" and hoteli2[hotel_key]["bazen"] != kriterijumi["bazen"]):
                        hoteli2.pop(hotel_key)
                        
                    
                    if(hotel_key in hoteli2.keys() and kriterijum_key == "restoran" and hoteli2[hotel_key]["restoran"] != kriterijumi["restoran"]):
                        hoteli2.pop(hotel_key)
                        
                    
                    if(hotel_key in hoteli2.keys() and kriterijum_key == "prosecna_ocena" and hoteli2[hotel_key]["prosecna_ocena"] != eval(kriterijumi["prosecna_ocena"])):
                        hoteli2.pop(hotel_key)
                    
                
                


                   
                
                

    return get_hotel_table(hoteli2)