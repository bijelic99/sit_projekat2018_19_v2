import model.rezervacija
import model.hotel
import model.soba
import copy
import utility
from datetime import *


current_time = datetime.now()


def getHotels():
    return copy.deepcopy(model.hotel.svi_hoteli)

def make_date(str_date):
    date=str_date.split("-")
    given_date = datetime(eval(date[2].lstrip("0")),eval(date[1].lstrip("0")),eval(date[0].lstrip("0")))
    return given_date

def is_date_valid_for_new_res(date):
    try:
        
        given_date = make_date(date)
        if(given_date.year<current_time.year):
            raise Exception("Date not correct")
        elif(given_date.year == current_time.year and given_date.month < current_time.month):
            raise Exception("Date not correct")
        elif(given_date.year == current_time.year and given_date.month == current_time.month and given_date.day < current_time.day):
            raise Exception("Date not correct")

    except:
        return False
    return True

def get_last_day(prijava,days):
    tdelta = timedelta(days=days)
    return prijava+tdelta

def get_available_rooms(hotel_id,datum_prijave,datum_odjave):
    
    rezervacije = model.rezervacija.sve_rezervacije
    sobe= model.soba.ucitaj_sobe_za_hotel(hotel_id)
    available_rooms = []
    taken_rooms = []
    #pomocu lambda izraza filter vraca samo one rezervacije sa istim hotel_id

    
    rezervacije=list(filter(lambda rezervacija: True if rezervacija["hotel_id"]==hotel_id else False, rezervacije.values()))
    #filtriraju se rezervacije tako da ostanu samo one koje se poklapaju
    #rezervacije=list(filter(lambda rezervacija: True if (((rezervacija["datum_odjave"]-datum_prijave).days>0) ) or ((datum_odjave-rezervacija["datum_prijave"]).days>0) else False, rezervacije))
    rezervacije = list(filter(lambda rezervacija: not (rezervacija["datum_odjave"]<=datum_prijave or rezervacija["datum_prijave"] >= datum_odjave),rezervacije))
    for r in rezervacije:
        for soba in r["rezervisane_sobe"]:
            taken_rooms.append(soba)
    #utility.log_something(taken_rooms)
    for soba in sobe:
        if (soba not in taken_rooms):
            available_rooms.append(soba)

    return available_rooms

def make_rooms_readable_dict(rooms=[]):
    readable_dict={}
    for room in rooms:
        readable_dict[room["id"]]=room_to_str(room)
    return readable_dict

def room_to_str(soba):
    da_ne={
        0 : "ne",
        1 : "da"
    }
    s=""
    s+=("hotel id: "+str(soba["hotel_id"])).center(14)
    s+=("br.sobe: "+str(soba["room_number"])).center(14)
    s+=("br.kreveta: "+str(soba["no_of_beds"])).center(14)
    s+=("tip: "+model.soba.room_type[soba["type"]]).center(18)
    s+=("klima: "+da_ne[soba["ac"]]).center(10)
    s+=("tv: "+da_ne[soba["tv"]]).center(10)
    s+=("balkon: "+da_ne[soba["balcony"]]).center(10)
    s+=("kupatilo: "+model.soba.kupatilo[soba["bathroom"]]).center(12)
    s+=("cena: "+str(soba["price_per_night"])).center(10)



    return s

def make_hotels_readable_dict(hotels={}):
    to_ret={}
    to_ret=add_hotel_keys(to_ret)
    for key in hotels.keys():
        to_ret[key]=hotel_to_str(hotels[key])
    return to_ret

def add_hotel_keys(dict_to_add):
    s=""
    s+="ime".center(20)
    s+="adresa".center(20)
    s+="bazen".center(14)
    s+="restoran".center(14)
    s+="ocena".center(10)
    s+="broj soba".center(12)
    dict_to_add["ID"]=s

    return dict_to_add

def hotel_to_str(hotel):
    da_ne={
        0 : "ne",
        1 : "da"
    }
    s=""
    s+=(hotel["name"]).center(20)
    s+=(hotel["address"]).center(20)
    s+=(da_ne[hotel["bazen"]]).center(14)
    s+=(da_ne[hotel["restoran"]]).center(14)
    s+=(str(hotel["prosecna_ocena"])).center(10)
    s+=(str(len(hotel["sobe"]))).center(12)

    return s



def napravi_rezervaciju(rezervacija):
    rezervacija["datum_i_vreme_rezervacije"]=datetime.now()
    sobe_ids = rezervacija["rezervisane_sobe"]
    sobe_objects = []
    for id in sobe_ids:
        sobe_objects.append(model.soba.get_soba(id))
    rezervacija["rezervisane_sobe"] = sobe_objects
    model.rezervacija.upisi_rezervaciju(rezervacija)

def get_users_reservations(username):
    lst=[]
    lst=list(filter(lambda rezervacija: rezervacija["username_korisnika"]==username,model.rezervacija.sve_rezervacije.values()))

    return lst

def make_readable_dict(rezervacije_list):
    ret_dict={}
    ret_dict=add_rez_keys(ret_dict)
    for rezervacija in rezervacije_list:
        ret_dict[rezervacija["id"]]=rezervacija_to_str(rezervacija)
    return ret_dict
    
def add_rez_keys(dict_to_add):
    s=""
    s+="id".center(10)
    s+="rezervisano dana".center(20)
    s+="datum prijave".center(20)
    s+="datum odjave".center(20)
    s+="username".center(16)
    s+="status".center(16)
    s+="ocena hotela".center(18)
    s+="hotel".center(20)
    dict_to_add["ID"]=s

    return dict_to_add

def rezervacija_to_str(rezervacija):
    s=""
    s+=str(rezervacija["id"]).center(10)
    s+=(rezervacija["datum_i_vreme_rezervacije"].strftime("%d-%m-%Y")).center(20)
    s+=(rezervacija["datum_prijave"].strftime("%d-%m-%Y")).center(20)
    s+=(rezervacija["datum_odjave"].strftime("%d-%m-%Y")).center(20)
    s+=(rezervacija["username_korisnika"]).center(16)
    s+=(model.rezervacija.statusi_rezervacije[rezervacija["status_rezervacije"]]).center(16)
    s+=(str(rezervacija["ocena_hotela"])).center(18)
    s+=(model.hotel.svi_hoteli[rezervacija["hotel_id"]]["name"]).center(20)
    return s

def get_users_past_reservations(username):
    rezervacije = []
    rezervacije = get_users_reservations(username)
    rezervacije = list(filter(lambda rezervacija : rezervacija["status_rezervacije"]==2,rezervacije))
    return rezervacije

def get_users_current_reservations(username):
    rezervacije = []
    rezervacije = get_users_reservations(username)
    rezervacije = list(filter(lambda rezervacija : rezervacija["status_rezervacije"]==1,rezervacije))
    return rezervacije

def get_users_future_reservations(username):
    rezervacije = []
    rezervacije = get_users_reservations(username)
    rezervacije = list(filter(lambda rezervacija : rezervacija["status_rezervacije"]==0,rezervacije))
    return rezervacije

def oceni_hotel(rezervacija,ocena=0):
    print(rezervacija)
    rezervacija["ocena_hotela"]=ocena
    model.rezervacija.izmeni_rezervaciju(rezervacija)
    model.rezervacija.oceni_hotele()
    return False 






    


