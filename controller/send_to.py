# Morao sam praviti ovaj modul zbog circular dependency load

import view.admin_view 
import view.registrovan_view 
import view.recepcioner_view 
import view.ne_registrovan_view 

def send_to_view(user={"role" : 0}):
    """
    podela={
                0 : view.ne_registrovan_view.show,
                1 : view.registrovan_view.show,
                2 : view.recepcioner_view.show,
                3 : view.admin_view.show
            }
    
    podela[user["role"]](user)
    
    #/|\
    # |
    #is nicer, doesnt work
    """
    if(user["role"] == 0):
        view.ne_registrovan_view.show(user)
    elif(user["role"] == 1):
        view.registrovan_view.show(user)
    elif(user["role"] == 2):
        view.recepcioner_view.show(user)
    elif(user["role"] == 3):
        view.admin_view.show(user)
